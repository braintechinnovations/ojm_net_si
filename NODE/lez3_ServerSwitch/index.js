const http = require("http");

const indirizzo = "127.0.0.1"       //localhost
const porta = 4001

const server = http.createServer((req, res) => {
    switch(req.url){
        case "/":
            res.end("Sono la pagina HOME");
            break;
        case "/contatti":
            res.end("Sono la pagina CONTATTI");
            break
        case "/servizi":
            res.end("Sono la pagina SERVIZI");
            break
        default:
            res.statusCode = 404;
            res.end("Ooops... pagina non trovata!s");
            break
    }
})

server.listen(porta, indirizzo, () => {
    console.log(`Sono in ascolto sulla porta ${indirizzo}:${porta}`)
})