const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost');

const dbName = "SitoNotizie"
const db = mongoose.connect(`mongodb+srv://mongouser:cicciopasticcio@cluster0.bkzky.mongodb.net/${dbName}?retryWrites=true&w=majority`, {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})

// FindAll -> SELECT * FROM BlogPost
// BlogPost.find({}, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// // SELECT * FROM BlogPosts WHERE Titolo = "Prova di primo articolo"
// BlogPost.find({
//     title: "Prova di primo articolo"
// }, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// // SELECT * FROM BlogPosts WHERE Titolo LIKE "Prova%"
// BlogPost.find({
//     title: /^Prova/
// }, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// // SELECT * FROM BlogPosts WHERE Titolo LIKE "%articolo"
// BlogPost.find({
//     title: /articolo$/
// }, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// // SELECT * FROM BlogPosts WHERE Titolo LIKE "%im%"
BlogPost.find({
    title: /i/
}, (error, elenco) => {
    if(!error)
        console.log(elenco)
    else
        console.log(error)
})
