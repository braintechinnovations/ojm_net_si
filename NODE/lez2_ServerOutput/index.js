const http = require("http");

const indirizzo = "127.0.0.1"       //localhost
const porta = 4001

const server = http.createServer((req, res) => {
    
    // console.log(req.url);
    // console.log(req.method);

    let responso = {
        urlRiferimento: req.url,
        metodoRichiesto: req.method
    }
    
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/json")
    res.end(JSON.stringify(responso));
})

server.listen(porta, indirizzo, () => {
    console.log(`Sono in ascolto sulla porta ${indirizzo}:${porta}`)
})