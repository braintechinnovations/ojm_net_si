const express = require('express')
const app = express()

const porta = 4001

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

//ROTTE
app.get("/", (req, res) => {
    res.end("Sono la Home")
})

app.get("/contatti", (req, res) => {
    res.end("Sono il Contattami")
})

app.get("/servizi", (req, res) => {
    res.end("Sono il Servizi")
})