const express = require('express')
const app = express()

const porta = 4005;

app.listen(porta, () =>{
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

app.get("/", (req, res) => {
    res.end("Prova")
})