const http = require("http");

const indirizzo = "127.0.0.1"       //localhost
const porta = 4001

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.end("<h1>CIAO GIOVANNI</h1>");

    console.log("Richiesta")
})

server.listen(porta, indirizzo, () => {
    console.log("Sono in ascolto sulla porta " + porta)
})