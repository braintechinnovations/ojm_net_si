// npm start

const express = require('express')
const mongoose = require('mongoose')
const Student = require('./models/Student')
const bodyParser = require('body-parser')
const cors = require('cors')                    //Aggiunta gestione CORS
const app = express()
const porta = 4001
const dbName = "Universita"

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.use(cors())                                 //Aggiunta gestione CORS

const db = mongoose.connect(`mongodb+srv://mongouser:cicciopasticcio@cluster0.bkzky.mongodb.net/${dbName}?retryWrites=true&w=majority`, {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

app.get("/api/studenti", (req, res) => {
    Student.find({}, (error, elenco) => {
        if(!error)
            res.json(elenco)
        else
            res.status(500)
    })
})

app.get("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findById(req.params.idstudente, (error, item) => {
        if(!error)
            res.status(200).json(item)
        else
            res.status(500).end()
    })
})

app.delete("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findByIdAndDelete(req.params.idstudente, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"})
        else
            res.status(500).end()
    })
})

// app.post("/api/studenti/inserisci", (req, res) => {
//     console.log(req.body)

//     Student.create(req.body, (error, item) => {
//         if(!error)
//             res.status(200).json(item)
//         else
//             res.status(500).end()
//     })
// })

app.post("/api/studenti/inserisci", async (req, res) => {
   let stud = await Student.create(req.body)

   if(stud)
        res.json(stud);
    else
        res.status(500).end()
})

app.put("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findByIdAndUpdate(req.params.idstudente, req.body, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"})
        else
            res.status(500).end()
    })
})