const express = require('express')
const app = express()

const porta = 4001

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

//ROTTE
app.get("/", (req, res) => {
    let persona = {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
    }

    res.json(persona);
})

app.get("/persona", (req, res) => {
    let persona = {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
    }

    res.status(404).json(null)
})

app.get("/lista", (req, res) => {
    let elenco = [
        { 
            nome: "Giovanni",
            cognome: "Pace",
        },
        { 
            nome: "Mario",
            cognome: "Rossi",
        },
        { 
            nome: "Valeria",
            cognome: "Verdi",
        },

    ]

    res.json(elenco)
})