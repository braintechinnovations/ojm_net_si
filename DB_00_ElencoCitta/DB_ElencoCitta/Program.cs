﻿using DB_ElencoCitta.Controllers;
using System;

namespace DB_ElencoCitta
{
    class Program
    {
        static void Main(string[] args)
        {
            CittaController gestore = new CittaController();
            gestore.stampaCitta();

            Console.WriteLine("-------------------------");

            //gestore.inserisciCitta("Catanzaro", "CZ");
            //gestore.stampaCitta();

            //gestore.eliminaCittaPerId(5);
            //gestore.stampaCitta();

            gestore.modificaCittaPerId(7, "Frosinone", "FR");
            gestore.stampaCitta();

            Console.ReadLine();

            /*
             * Creare un sistema di gestione di contatti, che memorizzi:
             * Nome
             * Cognome
             * Telefono (UNIVOCO)
             * 
             * - CRUD con DB
             * - Evitare l'inserimento di elementi già presenti in DB (evitare i metodi read generici)
             */

        }
    }
}
