﻿using DB_ElencoCitta.DAL;
using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_ElencoCitta.Controllers
{
    public class CittaController
    {
        public static IConfiguration configurazione;

        public CittaController()
        {
            //Leggo la configurazione ed inserirla nella variabile "configurazione"

            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            //var builder = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            configurazione = builder.Build();
        }

        public void stampaCitta()
        {
            CittaDAL cittadal = new CittaDAL(configurazione);

            List<Citta> elenco = cittadal.GetListaCitta();

            foreach (Citta item in elenco)
            {
                Console.WriteLine(item);
            }
        }

        public void inserisciCitta(string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittadal = new CittaDAL(configurazione);

            if (cittadal.InsertCitta(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }

        public void eliminaCittaPerId(int varId)
        {
            CittaDAL cittadal = new CittaDAL(configurazione);

            if(cittadal.DeleteCitta(varId))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }

        public void modificaCittaPerId(int varId, string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Id = varId,
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittadal = new CittaDAL(configurazione);

            if (cittadal.UpdateCitta(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
    }
}
