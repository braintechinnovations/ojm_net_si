﻿using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_ElencoCitta.DAL
{
    class CittaDAL
    {
        private string stringaConnessione;

        public CittaDAL(IConfiguration config)
        {
            stringaConnessione = config.GetConnectionString("ServerLocale");
        }

        public List<Citta> GetListaCitta()
        {
            List<Citta> elencoTemp = new List<Citta>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                string query = "SELECT cittaID, nome, prov FROM Citta";
                SqlCommand comm = new SqlCommand(query, connessione);

                connessione.Open();

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Citta temp = new Citta()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Provincia = reader[2].ToString()
                    };

                    elencoTemp.Add(temp);
                }

                connessione.Close();
            }

            return elencoTemp;
        }
        //LEGGI LE CITTA DAL DB

        public bool InsertCitta(Citta objCitta)
        {
            bool risultato = false;

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                //SqlCommand comm = new SqlCommand(query, connessione);

                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Citta (nome, prov) VALUES (@nomeCitta, @provCitta)";
                comm.Parameters.AddWithValue("@nomeCitta", objCitta.Nome);
                comm.Parameters.AddWithValue("@provCitta", objCitta.Provincia);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    risultato = true;

            }

            return risultato;
        }

        public bool DeleteCitta(int varId)
        {
            bool risultato = false;

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;

                comm.CommandText = "DELETE FROM Citta WHERE cittaID = @idCitta";
                comm.Parameters.AddWithValue("@idCitta", varId);

                connessione.Open();

                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    risultato = true;
            }

            return risultato;
        }

        public bool UpdateCitta(Citta objCitta)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;

                comm.CommandText = "UPDATE Citta SET nome = @varNome, prov = @varProv WHERE cittaID = @varId";
                comm.Parameters.AddWithValue("@varNome", objCitta.Nome);
                comm.Parameters.AddWithValue("@varProv", objCitta.Provincia);
                comm.Parameters.AddWithValue("@varId", objCitta.Id);

                connessione.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}
