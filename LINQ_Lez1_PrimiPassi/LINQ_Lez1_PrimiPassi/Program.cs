﻿using LINQ_Lez1_PrimiPassi.Classes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ_Lez1_PrimiPassi
{
    class Program
    {
        static void stampaRisultato(List<Utente> elenco)
        {
            foreach(Utente item in elenco)
            {
                Console.WriteLine(item);
            }
        }

        static void Main(string[] args)
        {
            List<Utente> elenco = new List<Utente>();

            elenco.Add(new Utente() { Id = 1, Nominativo = "Giovanni Pace", Eta = 35, Sesso = "M" });
            elenco.Add(new Utente() { Id = 2, Nominativo = "Mario Rossi", Eta = 15, Sesso = "M" });
            elenco.Add(new Utente() { Id = 3, Nominativo = "Valeria Verdi", Eta = 25, Sesso = "F" });
            elenco.Add(new Utente() { Id = 4, Nominativo = "Marika Viola", Eta = 41, Sesso = "F" });

            //SELECT * FROM elenco
            //var risultato = elenco.ToList();                   //Riversa il contenuto di elenco in un nuovo contenitore di RIsultato

            //SELECT * FROM elenco WHERE nominativo LIKE "%Ri%" AND sesso == "M"
            //var risultato = elenco.Where(o => o.Nominativo.ToLower().Contains("Ri".ToLower()) && o.Sesso == "M").ToList();
            //stampaRisultato(risultato);

            //SELECT Id, Nominativo FROM elenco
            //var risultato = elenco.Select(s => new Utente(){ Id = s.Id, Nominativo = s.Nominativo}).ToList();
            //stampaRisultato(risultato);

            //var risultato = elenco.Select(s => new { Identificatore = s.Id, Nomenclatura = s.Nominativo });
            //foreach (var item in risultato)
            //{
            //    Console.WriteLine(item);
            //}

            //Flattizza delle property di tipo contenitore
            //var risultato = elenco.SelectMany(o => o.Interessi).ToList();

            //int conteggio = elenco.Where(o => o.Nominativo.ToLower().Contains("Ri".ToLower())).Count();
            //Console.WriteLine(conteggio);
            //long conteggio = elenco.Where(o => o.Nominativo.ToLower().Contains("Ri".ToLower())).LongCount();
            //Console.WriteLine(conteggio);

            //TOP 1 SELECT *  FROM elenco WHERE Id == 32
            //Utente risultato = elenco.Where(u => u.Id == 32).First();
            //Console.WriteLine(risultato);

            //Utente risultato = elenco.Where(u => u.Id == 32).FirstOrDefault();
            //Console.WriteLine(risultato);

            //Sottoinsiemi
            //var risultato = elenco.Take(2).ToList();
            //var risultato = elenco.Skip(2).ToList();


            //var risultato = elenco.Skip(1).Take(2).ToList();
            //stampaRisultato(risultato);

            //var risultato = elenco.Where(u => u.Id == 32).ToList();
            //var risultato = from item in elenco where item.Id == 3 select item;
            //stampaRisultato(risultato.ToList());

            var risultato = elenco
                .Where(o => o.Nominativo.ToLower().Contains("Ri".ToLower()))
                .Where(k => k.Eta >= 18) 
                .ToList();
            stampaRisultato(risultato);

        }
    }
}
