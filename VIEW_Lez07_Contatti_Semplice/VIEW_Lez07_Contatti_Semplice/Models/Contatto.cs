﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VIEW_Lez07_Contatti_Semplice.Models
{
    public class Contatto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Telefono { get; set; }

        public override string ToString()
        {
            return $"{Nome} - {Cognome} - {Telefono}";
        }
    }
}
