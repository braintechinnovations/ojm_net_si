﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VIEW_Lez07_Contatti_Semplice.Controllers;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice
{
    /// <summary>
    /// Logica di interazione per ModificaContatto.xaml
    /// </summary>
    public partial class ModificaContatto : Window
    {
        private Contatto contattoSelezionato;

        public ModificaContatto(Contatto objContatto)
        {
            InitializeComponent();

            contattoSelezionato = objContatto;

            inNome.Text = contattoSelezionato.Nome;
            inCognome.Text = contattoSelezionato.Cognome;
            inTelefono.Text = contattoSelezionato.Telefono;
        }

        private void Button_Click_Modifica(object sender, RoutedEventArgs e)
        {
            if ((new ContattoController()).ModificaContatto(
                contattoSelezionato.Id,
                inNome.Text,
                inCognome.Text,
                inTelefono.Text))
            {
                MessageBox.Show("Modifica effettuata con successo", "Tutto ok!", MessageBoxButton.OK, MessageBoxImage.Information);

                this.Close();
            }

            else
                MessageBox.Show("Non sono riuscito ad effettuare le operaizoni richieste", "Errore!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Button_Click_Elimina(object sender, RoutedEventArgs e)
        {
            if((new ContattoController()).EliminaContatto(contattoSelezionato.Id))
            {
                MessageBox.Show("Eliminazione effettuata con successo", "Tutto ok!", MessageBoxButton.OK, MessageBoxImage.Information);

                this.Close();
            }

            else
                MessageBox.Show("Non sono riuscito ad effettuare le operaizoni richieste", "Errore!", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
