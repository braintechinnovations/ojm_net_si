﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice.DAL
{
    class ContattoDAL : InterfaceDAL<Contatto>
    {
        public static string stringaConnessione;

        public ContattoDAL(IConfiguration configurazione)
        {
            if(stringaConnessione == null)
                stringaConnessione = configurazione.GetConnectionString("ServerLocale");
        }

        public bool Delete(int varId)
        {
            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "DELETE FROM Contatti " +
                                        "WHERE rubricaID = @varId";

                cmd.Parameters.AddWithValue("@varId", varId);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }

        public bool Insert(Contatto t)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Contatti (nome, cognome, telefono) VALUES (@varNome, @varCognome, @varTelefono)";
                comm.Parameters.AddWithValue("@varNome", t.Nome);
                comm.Parameters.AddWithValue("@varCognome", t.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", t.Telefono);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    return true;
            }

            return false;
        }

        public List<Contatto> ReadAll()
        {
            List<Contatto> elenco = new List<Contatto>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT rubricaID, nome, cognome, telefono FROM Contatti";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };

                    elenco.Add(temp);
                }
            }

            return elenco;
        }

        public bool Update(Contatto t)
        {
            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "UPDATE Contatti SET " +
                                        "nome = @varNome, " +
                                        "cognome = @varCognome, " +
                                        "telefono = @varTelefono " +
                                        "WHERE rubricaID = @varId";

                cmd.Parameters.AddWithValue("@varNome", t.Nome);
                cmd.Parameters.AddWithValue("@varCognome", t.Cognome);
                cmd.Parameters.AddWithValue("@varTelefono", t.Telefono);
                cmd.Parameters.AddWithValue("@varId", t.Id);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}
