function inserisci(){
    let varProdotto = document.getElementById("inProdotto").value;

    if(varProdotto.length == 0){
        alert("Non puoi inserire un prodotto inesistente!");
        return;
    }

    if(isInElenco(varProdotto)){
        alert("Non puoi inserire un prodotto già esistente!");
        return;
    }

    let prod = {
        nome: varProdotto
    }

    elenco.push(prod);
    localStorage.setItem("ListaSpesa", JSON.stringify(elenco));
    stampaProdotti();

    document.getElementById("inProdotto").focus()
}

function isInElenco(varNome){
    for(let [indice, item] of elenco.entries()){
        if(item.nome.toLowerCase() == varNome.toLowerCase())
            return true;
    }

    return false;
}

function elimina(varNome){
    for(let [index, item] of elenco.entries()){
        if(item.nome == varNome)
            elenco.splice(index, 1)
    }

    localStorage.setItem("ListaSpesa", JSON.stringify(elenco));
    stampaProdotti();
}

function stampaProdotti(){
    let contenuto = "";
    for(let [index, item] of elenco.entries()){
        contenuto += `  <button type="button" class="list-group-item list-group-item-action" onclick="elimina('${item.nome}')">
                            ${item.nome}
                        </button>`;
    }

    document.getElementById("contenitore-lista").innerHTML = contenuto;
}

if(localStorage.getItem("ListaSpesa") == null)
    localStorage.setItem("ListaSpesa", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("ListaSpesa"));
stampaProdotti();