function inserisci(){
    let varNome = document.getElementById("inNome").value;
    let varCognome = document.getElementById("inCognome").value;
    let varEmail = document.getElementById("inEmail").value;

    let persona = {
        nome: varNome,
        cognome: varCognome,
        email: varEmail
    }

    // let elemento =  "<tr>";
    // elemento +=         "<td>" + persona.nome + "</td>";  
    // elemento +=         "<td>" + persona.cognome + "</td>";  
    // elemento +=         "<td>" + persona.email + "</td>";  
    // elemento +=     "</tr>";

    let elemento = `<tr>
        <td>${persona.nome }</td>
        <td>${persona.cognome}</td>
        <td>${persona.email}</td>
    </tr>`;

    document.getElementById("contenutoTabella").innerHTML += elemento;
}

/*

    CREARE UN FORM comprensivo di: Nome, cognome, matricola studente ed anno di iscrizione
    I dati verranno inseriti all'interno di una tabella le cui righe dovranno sottostare alle seguenti direttive:

    - Se sono iscritto da più di tre anni, colorare tutta la riga di blu (solo il testo)
    - Se sono iscritto da più di tre anni ma meno di cinque, colorare tutta la riga di verde (solo il testo)
    - Se sono iscritto da più di cinque anni, colorare tutta la riga di rosso (solo il testo)

*/