function iscrivimi(){
    let varNome = document.getElementById("inNome").value;
    let varCodFis = document.getElementById("inCodFis").value;
    let varIndirizzo = document.getElementById("inIndirizzo").value;
    let varCognome = document.getElementById("inCognome").value;
    let varDataNascita = document.getElementById("inDataNascita").value;
    let varNumFigli = document.getElementById("inNumFigli").value;

    let persona = {
        nome: varNome,
        cognome: varCognome,
        cod_fis: varCodFis,
        indirizzo: varIndirizzo,
        data_nascita: varDataNascita,
        num_figli: varNumFigli
    }

    console.log(
        `Nome: ${persona.nome}\nCognome: ${persona.cognome}`
        );

    document.getElementById("inNome").value = "";
    document.getElementById("inCodFis").value = "";
    document.getElementById("inIndirizzo").value = "";
    document.getElementById("inCognome").value = "";
    document.getElementById("inDataNascita").value = "";
    document.getElementById("inNumFigli").value = "0";
}

/*
    Creare un form di iscrizione al corso in palestra...
    Fare l'output in console SE E SOLO SE tutti i campi sono riempiti con almeno 3 caratteri
*/