// var automobili = [
//     "Maserati",
//     "Lamborghini",
//     1234,
//     "FIAT"
// ];

// // console.log(automobili);
// // console.log(automobili.length);

// for(let i=0; i<automobili.length; i++){
//     console.log(typeof automobili[i]);
// }

//------------------------------------------

// var input = prompt("Inserisci il nome");
// console.log(input);

//TASK: Creare un piccolo programma che prende in input il nome di un'automobile
//e restituisce se questa c'è o no! DAL PROMPT (permettere l'uscita alla digitazione di Q)

var automobili = [
    "Maserati",
    "Lamborghini",
    "BMW",
    "FIAT",
    "Lancia"
];

let inserimentoAbilitato = true;

while(inserimentoAbilitato){
    let input = prompt("Inserisci il nome dell'auto o digita Q per uscire");

    if(input == "Q")
        break;


    let trovato = false;
    for(let i=0; i<automobili.length; i++){
        if(automobili[i] == input)
            console.log("Ho trovato");
    }

    // if(trovato)
    //     console.log("Ho trovato");
    // else
    //     console.log("Non trovato")
}