/**Funzione dedicata al solo inserimento dei valori */
function inserisci(){
    let varNome = document.getElementById("inNome").value;
    let varCognome = document.getElementById("inCognome").value;
    let varCodFis = document.getElementById("inCodFis").value;

    let persona = {
        nome: varNome,
        cognome: varCognome,
        cod_fis: varCodFis,
    }

    if(!verificaPresenza(varCodFis))
        elenco.push(persona)
    else
        alert("Attenzione, codice fiscale già presente")

    stampa();
}

function verificaPresenza(varCodFisIniettato){
    for(let i=0; i<elenco.length; i++){
        if(elenco[i].cod_fis == varCodFisIniettato)
            return true;
    }

    return false;
}

/**Funzione dedicata alla sola stampa dei valori */
function stampa(){
    document.getElementById("contenuto-tabella").innerHTML = "";
    for(let i=0; i<elenco.length; i++){
        let elemento = `
            <tr>
                <td>${elenco[i].nome}</td>
                <td>${elenco[i].cognome}</td>
                <td>${elenco[i].cod_fis}</td>
            </tr>
        `;

        document.getElementById("contenuto-tabella").innerHTML += elemento;
    }
}

let elenco = [];