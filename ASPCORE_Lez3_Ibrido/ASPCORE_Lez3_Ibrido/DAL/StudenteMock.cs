﻿using ASPCORE_Lez3_Ibrido.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez3_Ibrido.DAL
{
    public class StudenteMock : InterfaceDAL<Studente>
    {
        public IEnumerable<Studente> GetAll()
        {
            return new List<Studente> { 
                new Studente(){Id = 1, Nome = "Giovanni", Cognome = "Pace", Matricola = "AB12345"},
                new Studente(){Id = 2, Nome = "Mario", Cognome = "Rossi", Matricola = "AB12346"},
                new Studente(){Id = 3, Nome = "Valeria", Cognome = "Verdi", Matricola = "AB12347"},
            };
        }

        public Studente GetById(int varId)
        {
            return new Studente() { Id = 5, Nome = "Mario", Cognome = "Rossi", Matricola = "AB12346" };
        }

        public bool Insert(Studente t)
        {
            return true;
        }
    }
}
