﻿using ASPCORE_Lez3_Ibrido.DAL;
using ASPCORE_Lez3_Ibrido.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez3_Ibrido.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Lista()
        {
            StudenteMock dalStudente = new StudenteMock();
            List<Studente> elenco = (List<Studente>)dalStudente.GetAll();

            return View(elenco);
        }

        public IActionResult Dettaglio(int varId)
        {
            StudenteMock dalStudente = new StudenteMock();
            return View(dalStudente.GetById(varId));
        }

        public IActionResult Inserisci()
        {
            return View();
        }

        [HttpPost]
        public RedirectResult EffettuaInserimento(Studente stud)
        {
            //TODO: Inserisci nel DB

            StudenteMock dalStudente = new StudenteMock();
            if (dalStudente.Insert(stud))
                return Redirect("/Home/Lista");
            else
                return Redirect("/Home/Dettaglio/Errore");
        }










        //Versione REST
        [HttpPost]
        public ActionResult<IEnumerable<Studente>> ListaStudentiRest()
        {
            StudenteMock dalStudente = new StudenteMock();
            return Ok(dalStudente.GetAll());
        }
    }
}
