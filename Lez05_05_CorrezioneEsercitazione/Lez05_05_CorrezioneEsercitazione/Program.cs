﻿using Lez05_05_CorrezioneEsercitazione.Classes;
using System;
using System.Collections.Generic;

namespace Lez05_05_CorrezioneEsercitazione
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
         * Realizzare un sistema di gestione dell'anagrafica studenti.
         * I valori da inserire, e consultare, sono i seguenti: 
         * Matricola, Nome, Città di provenienza, totale CFU conseguiti
         * tra 0 e 180 (0<cfu<60 - primo anno, 60< = cfu < 120 secondo anno, cfu > = 120 terzo anno )
         * Avviando l'applicazione, l'utente si troverà di fronte ad un menù a scelta, 
         * con le seguenti options:

            * 1. inserimento nuovo studente
             * 2. aggiornamento dati studente
             * 3. eliminazione studente
             * 4. visualizzazione dettaglio scheda studente
             * 5. visualizzazione lista studenti
             * 6. verifica risultati studente
             * 7. Uscita dall'applicazione
        */

            //Studente studUno = new Studente("Giovanni Pace", 123456, "Avezzano", 63);
            //Console.WriteLine(studUno);

            bool inserimentoAbilitato = true;
            List<Studente> elenco = new List<Studente>();

            elenco.Add(new Studente("Giovanni Pace", 12345, "Avezzano", 150));
            elenco.Add(new Studente("Mario Rossi", 12346, "Roma", 130));
            elenco.Add(new Studente("Valeria Verdi", 12347, "Milano", 60));

            while (inserimentoAbilitato)
            {
                Console.WriteLine(
                        "1.inserimento nuovo studente\n" +
                        "2.aggiornamento dati studente\n" +
                        "3.eliminazione studente\n" +
                        "4.visualizzazione dettaglio scheda studente\n" +
                        "5.visualizzazione lista studenti\n" +
                        "6.verifica risultati studente\n" +
                        "7.Uscita dall'applicazione\n"
                );
                string input = Console.ReadLine();
                int inputConvertito = 0;
                bool sceltaAbilitata = true;

                //FormatException
                try
                {
                    inputConvertito = Convert.ToInt32(input);
                } catch (FormatException e)
                {
                    Console.WriteLine("Caratteri non consentiti, riprova!");
                    sceltaAbilitata = false;
                }

                string nominativo, matricola, citta, cfu;
                int matrConvertita, cfuConvertito;

                if (sceltaAbilitata)
                    switch (inputConvertito)
                    {
                        case 1:             //Inserimento nuovo studente
                            Console.WriteLine("Nominativo:");
                            nominativo = Console.ReadLine();

                            Console.WriteLine("Matricola:");
                            matricola = Console.ReadLine();
                            matrConvertita = Convert.ToInt32(matricola);

                            Console.WriteLine("Città:");
                            citta = Console.ReadLine();

                            Console.WriteLine("Cfu:");
                            cfu = Console.ReadLine();
                            cfuConvertito = Convert.ToInt32(cfu);

                            Studente temp = new Studente(nominativo, matrConvertita, citta, cfuConvertito);
                            elenco.Add(temp);

                            Console.WriteLine("Operazione effettuata con successo!\n");
                            break;
                        case 2:
                            Console.WriteLine("Inserisci la matricola di cui vuoi i dettagli");
                            matricola = Console.ReadLine();
                            matrConvertita = Convert.ToInt32(matricola);

                            foreach (Studente item in elenco)
                            {
                                if (matrConvertita.Equals(item.Matricola))
                                {
                                    //Bla bla bla...

                                    break;
                                }
                            }
                            break;

                            break;
                        case 3:             //Eliminazione di uno studente
                            Console.WriteLine("Inserisci la matricola da eliminare");
                            matricola = Console.ReadLine();
                            matrConvertita = Convert.ToInt32(matricola);

                            foreach (Studente item in elenco)
                            {
                                if (matrConvertita.Equals(item.Matricola))
                                {
                                    elenco.Remove(item);
                                    break;
                                }
                            }

                            break;
                        case 4:
                            Console.WriteLine("Inserisci la matricola di cui vuoi i dettagli");
                            matricola = Console.ReadLine();
                            matrConvertita = Convert.ToInt32(matricola);

                            foreach (Studente item in elenco)
                            {
                                if (matrConvertita.Equals(item.Matricola))
                                {
                                    Console.WriteLine(item);
                                    break;
                                }
                            }
                            break;
                        case 5:             //visualizzazione lista studenti
                            Console.WriteLine("---------------------------------");
                            foreach(Studente item in elenco)
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine("---------------------------------");

                            break;
                        case 6:
                            Console.WriteLine("Inserisci la matricola di cui vuoi i dettagli");
                            matricola = Console.ReadLine();
                            matrConvertita = Convert.ToInt32(matricola);

                            foreach (Studente item in elenco)
                            {
                                if (matrConvertita.Equals(item.Matricola))
                                {
                                    Console.WriteLine(item.verificaAnno());
                                    break;
                                }
                            }
                            break;
                        case 7:
                            inserimentoAbilitato = !inserimentoAbilitato;
                            break;
                        default:
                            Console.WriteLine("Comando non trovato! ;(");
                            break;
                    }
            }

        }
    }
}
