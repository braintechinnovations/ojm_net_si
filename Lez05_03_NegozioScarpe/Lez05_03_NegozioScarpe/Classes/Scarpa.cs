﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_03_NegozioScarpe.Classes
{
    public class Scarpa
    {
        public string Codice { get; set; }
        public string Marca { get; set; }
        public string Materiale { get; set; }
        public float Misura { get; set; }

        public override string ToString()
        {
            return $"{Codice} - {Marca}\n" +
                $"Materiale: {Materiale}" +
                $"Misura: {Misura}";
        }
    }
}
