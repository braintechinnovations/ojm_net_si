﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_Parziali.Classes
{
    public partial class Persona
    {
        private string codice = "N.D.";

        public string Nome { get; set; }
        public string Cognome { get; set; }
    }

    public partial class Persona
    {
        public string CodFis { get; set; }

        public override string ToString()
        {
            return $"{Nome} {Cognome} - {CodFis} - {codice} - {Sesso} - {Eta}";
        }
    }


}
