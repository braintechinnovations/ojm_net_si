﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_03_Astrazione.Classes
{
    public abstract class Animale
    {
        protected bool haIlPelo;

        //public bool HaIlPelo
        //{
        //    get { return haIlPelo; }
        //    set { haIlPelo = value; }
        //}


        public int NumZampe { get; set; }
        public bool Vola { get; set; }

        public abstract string VersoEmesso();
    }
}
