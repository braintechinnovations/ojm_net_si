﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_03_Astrazione.Classes
{
    class Gatto : Animale
    {
        public string Colore { get; set; }
        public Gatto()
        {
            haIlPelo = true;
        }

        public override string VersoEmesso()
        {
            return "Meow";
        }
    }
}
