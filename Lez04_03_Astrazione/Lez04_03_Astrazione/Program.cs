﻿using Lez04_03_Astrazione.Classes;
using System;

namespace Lez04_03_Astrazione
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animale tigre = new Animale();

            Gatto bu = new Gatto();
            Console.WriteLine(bu.VersoEmesso());

            Coccodrillo drillo = new Coccodrillo();
            Console.WriteLine(drillo.VersoEmesso());

        }
    }
}
