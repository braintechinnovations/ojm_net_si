﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_02_NamespaceEInfoHiding.Classes
{
    public class Moto
    {
        private string marca;
        private int cilindrata;
        private string colore;

        public Moto()
        {
            Console.WriteLine("Ho utilizzato il costruttore di Default");
        }
        public Moto(string varMarca, string varColore)
        {
            this.marca = varMarca;
            this.colore = varColore;

            Console.WriteLine("Ho utilizzato il costruttore con due parametri");
        }

        public Moto(string varMarca, string varColore, int varCilindrata)
        {
            this.marca = varMarca;
            this.cilindrata = varCilindrata;
            this.colore = varColore;

            Console.WriteLine("Ho utilizzato il costruttore con parametri");
        }
        public Moto(int varCilindrata, string varMarca, string varColore)
        {
            this.marca = varMarca;
            this.cilindrata = varCilindrata;
            this.colore = varColore;

            Console.WriteLine("Ho utilizzato il costruttore con parametri");
        }

        public void stampaDettagli()
        {
            Console.WriteLine($"{this.marca} - {this.cilindrata} - {this.colore}");
        }
    }
}
