﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_02_NamespaceEInfoHiding.Classes
{
    public class Bicicletta
    {
        private static int contatore;

        private bool ammortizzata;
        private string marca;
        private string colore;
        public Bicicletta()
        {
            contatore++;
        }

        public Bicicletta(bool varAmmortizzata, string varMarca, string varColore)
        {
            this.ammortizzata = varAmmortizzata;
            this.marca = varMarca;
            this.colore = varColore;
            contatore++;
        }

        public static void visualizzaContatore()
        {
            Console.WriteLine(contatore);
        }
    }
}
