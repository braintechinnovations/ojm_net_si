﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_02_NamespaceEInfoHiding.Classes
{
    public class Automobile
    {
        private string marca = "N.D.";
        private int cilindrata = 0;
        private string colore = "N.D.";
        private int posti = 0;

        public void setCilindrata(int varCilindrata)
        {
            if (varCilindrata > 0)
            {
                this.cilindrata = varCilindrata;
            }
            else
            {
                Console.Write("Errore nell'inserimento della cilindrata");
            }
        }

        public void setMarca(string varMarca)
        {
            if (varMarca.Length == 0)
            {
                this.marca = "N.D.";
            }
            else
            {
                this.marca = varMarca;
            }
        }

        public void setPosti(int varPosti)
        {
            if(varPosti > 1)
            {
                this.posti = varPosti;
            }
            else
            {
                Console.Write("Errore nell'inserimento dei posti");
            }
        }

        public void setColore(string varColore)
        {
            this.colore = varColore;
        }

        public int getCilindrata()
        {
            return this.cilindrata;
        }
        public string getMarca()
        {
            return this.marca;
        }
        public string getColore()
        {
            return this.colore;
        }
        public int getPosti()
        {
            return this.posti;
        }

        public void stampaDettaglio()
        {
            Console.WriteLine($"{this.getMarca()} - {this.getPosti()} - {this.getColore()} - {this.getCilindrata()}");
        }
    }
}
