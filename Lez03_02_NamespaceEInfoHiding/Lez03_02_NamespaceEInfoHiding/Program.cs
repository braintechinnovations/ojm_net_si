﻿using Lez03_02_NamespaceEInfoHiding.Classes;
using System;

namespace Lez03_02_NamespaceEInfoHiding
{
    class Program
    {
        static void Main(string[] args)
        {
            //Automobile huracan = new Automobile();
            ////huracan.colore = "Gialla";
            ////huracan.cilindrata = -5400;
            ////huracan.marca = "Lamborghini";
            ////huracan.posti = 2;

            //huracan.setCilindrata(5400);
            //huracan.setColore("Giallo");
            ////huracan.setMarca("Lamborghini");
            //huracan.setPosti(2);

            ////huracan.stampaDettaglio();

            //Console.WriteLine($"Cilindrata: {huracan.getCilindrata()}");
            //Console.WriteLine($"Posti: {huracan.getPosti()}");
            //Console.WriteLine($"Marca: {huracan.getMarca()}");
            //Console.WriteLine($"Colore: {huracan.getColore()}");

            //huracan.stampaDettaglio();

            //--------------------------------------------------------------------

            //Moto monster = new Moto("Ducati", "Nero", 1200);
            //Moto xadv = new Moto("Honda", "Bianco", 750);
            //Moto graziella = new Moto(0, "Bianchi", "Bianca");

            //Moto nevada = new Moto();

            //Moto california = new Moto("Guzzi", "Grigio");

            //monster.stampaDettagli();
            //xadv.stampaDettagli();

            //--------------------------------------------------------------------

            //Bicicletta commencal = new Bicicletta();
            ////commencal.visualizzaContatore();

            //Bicicletta nukeproof = new Bicicletta();
            ////nukeproof.visualizzaContatore();

            //Bicicletta cannondale = new Bicicletta();
            ////cannondale.visualizzaContatore();

            //Bicicletta.visualizzaContatore();

            //--------------------------------------------------------------------

            Monopattino xiaomi = new Monopattino("123456");
            xiaomi.Marca = "Xiaomi";
            Console.WriteLine($"Marca: {xiaomi.Marca} - Telaio: {xiaomi.Telaio}" );

            Monopattino seagway = new Monopattino("123457");
            seagway.Marca = "";
            Console.WriteLine($"Marca: {seagway.Marca} - Telaio: {seagway.Telaio}");

        }
    }
}
