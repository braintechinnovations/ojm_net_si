﻿using System;

namespace Lez02_06_ContenitoriComplessi
{
    class Program
    {
        static void Main(string[] args)
        {
            //string[] elencoStudenti = {
            //    "Giovanni Pace 123456",
            //    "Mario Rossi 123457",
            //    "Valeria Verdi 123458"
            //};

            //string[][] elencoStudenti = { 
            //    new string[] { "Giovanni", "Pace", "123456" },
            //    new string[] { "Mario", "Rossi", "123457" },
            //    new string[] { "Valeria", "Verdi", "123458" },
            //};

            //for(int i=0; i<elencoStudenti.Length; i++)
            //{
            //    string[] arrayRiga = elencoStudenti[i];

            //    for(int j=0; j< arrayRiga.Length; j++)
            //    {
            //        Console.WriteLine(arrayRiga[j]);
            //    }
            //}


            //for(int i=0; i<elencoStudenti.Length; i++)
            //{
            //    for(int j=0; j< elencoStudenti[i].Length; j++)
            //    {
            //        Console.WriteLine(elencoStudenti[i][j]);
            //    }

            //    Console.WriteLine("");
            //}

            //---------------------------------------------------------
            //Fare la ricerca di una matricola

            string[][] elencoStudenti = {
                new string[] { "Giovanni", "Pace", "123456" },
                new string[] { "Mario", "Rossi", "123457" },
                new string[] { "Valeria", "Verdi", "123458" },
            };

            Console.Write("Inserisci il numero di matricola: ");
            string matricola = Console.ReadLine();

            for(int i=0; i<elencoStudenti.Length; i++)
            {
                if (matricola.Equals(elencoStudenti[i][2]))
                    Console.WriteLine($"{elencoStudenti[i][0]} {elencoStudenti[i][1]} - Matr. {elencoStudenti[i][2]}");

                //Console.WriteLine(elencoStudenti[i][2]);
            }


        }
    }
}
