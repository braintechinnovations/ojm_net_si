﻿using ASP_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Data
{
    interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);

        bool Insert(T obj);

        bool Delete(int varId);

        bool Update(T obj);
    }
}
