﻿using ASP_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Data
{
    public class MockContatti : InterfaceRepo<Contatto>
    {
        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Contatto> GetAll()
        {
            var elenco = new List<Contatto>
            {
                new Contatto() { Id = 1, Nome = "Giovanni", Cognome = "Pace", Telefono = "123456"},
                new Contatto() { Id = 2, Nome = "Mario", Cognome = "Rossi", Telefono = "123456"},
                new Contatto() { Id = 3, Nome = "Valeria", Cognome = "Verdi", Telefono = "123456"},
                new Contatto() { Id = 4, Nome = "Marika", Cognome = "Mariko", Telefono = "123456"},
            };

            return elenco;
        }

        public Contatto GetById(int varId)
        {
            var persona = new Contatto() { Id = 3, Nome = "Valeria", Cognome = "Verdi", Telefono = "VLRVRD" };
            return persona;
        }

        public bool Insert(Contatto obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(Contatto obj)
        {
            throw new NotImplementedException();
        }
    }
}
