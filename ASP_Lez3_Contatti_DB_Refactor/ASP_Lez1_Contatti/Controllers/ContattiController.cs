﻿using ASP_Lez1_Contatti.Data;
using ASP_Lez1_Contatti.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Controllers
{
    [Route("api/contatti")]
    [ApiController]
    public class ContattiController : Controller
    {
        private readonly SqlContatti _repository = new SqlContatti();

        [HttpPost, HttpGet]
        public ActionResult<IEnumerable<Contatto>> GetAllContatti()
        {
            var elenco = _repository.GetAll();
            return Ok(elenco);
        }

        [HttpPost("{id}"), HttpGet("{id}")]
        public ActionResult<Contatto> GetContatti(int id)
        {
            var contatto = _repository.GetById(id);
            return Ok(contatto);
        }

        //Poco flessibile con tanti dati, ricordo che URL può contenere al massimo 2038 caratteri!
        //[HttpGet("insert")]
        //public ActionResult InsertContatto(string varNome, string varCognome, string varTelefono)
        //{
        //    Contatto temp = new Contatto()
        //    {
        //        Nome = varNome,
        //        Cognome = varCognome,
        //        Telefono = varTelefono
        //    };

        //    if (_repository.InsertContatto(temp))
        //        return Ok("Successo!");
        //    else
        //        return Ok("Errore, non ho completato l'operazione");
        //}

        [HttpPost("insert")]
        public ActionResult InsertContatto(Contatto objContatto)
        {
            if (_repository.Insert(objContatto))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteContatto(int id)
        {
            if (_repository.Delete(id))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }

        [HttpPut("{id}")]
        public ActionResult UpdateContatto(Contatto objContatto, int id)
        {
            objContatto.Id = id;

            if (_repository.Update(objContatto))
                return Ok("Successo!");
            else
                return Ok("Errore, non ho completato l'operazione");
        }
    }
}
