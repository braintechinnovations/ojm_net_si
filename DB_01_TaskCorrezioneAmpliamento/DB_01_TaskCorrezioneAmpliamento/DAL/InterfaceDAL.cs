﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_01_TaskCorrezioneAmpliamento.DAL
{
    interface InterfaceDAL<T>
    {
        bool Insert(T t);

        List<T> ReadAll();

        T ReadById(int varId);

        bool Update(T t);
    }
}
