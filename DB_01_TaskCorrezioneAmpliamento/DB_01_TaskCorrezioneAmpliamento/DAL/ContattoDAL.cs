﻿using DB_01_TaskCorrezioneAmpliamento.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_01_TaskCorrezioneAmpliamento.DAL
{
    class ContattoDAL : InterfaceDAL<Contatto>
    {
        private static string connectionString;

        public ContattoDAL(IConfiguration configurazione)
        {
            if(connectionString == null)
                connectionString = configurazione.GetConnectionString("DatabaseTest");
        }

        public bool Insert(Contatto t)
        {
            throw new NotImplementedException();
        }

        public List<Contatto> ReadAll()
        {
            List<Contatto> risultato = new List<Contatto>();

            using (SqlConnection c = new SqlConnection(connectionString))
            {
                string query = "SELECT rubricaID, nome, cognome, telefono FROM Contatti";
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = query;
                cmd.Connection = c;

                c.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString(),
                    };

                    risultato.Add(temp);
                }
                c.Close();
            }

            return risultato;
        }

        public Contatto ReadById(int varId)
        {
            Contatto risultato = null;

            using (SqlConnection c = new SqlConnection(connectionString))
            {
                string query = "SELECT rubricaID, nome, cognome, telefono FROM Contatti WHERE rubricaID = @varId";
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@varId", varId);
                cmd.Connection = c;

                try
                {
                    c.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    reader.Read();

                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString(),
                    };

                    return temp;

                    c.Close();
                } catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException exSql)
                {
                    Console.WriteLine(exSql.Message);
                }

            }


            return risultato;
        }

        public bool Update(Contatto t)
        {
            if(t.Nome == null && t.Cognome == null && t.Telefono == null)
                return false;

            Contatto vecchio = ReadById(t.Id);

            if (t.Nome == vecchio.Nome && t.Cognome == vecchio.Cognome && t.Telefono == vecchio.Telefono)
                return false;

            if (t.Nome == null)
                t.Nome = vecchio.Nome;
            if (t.Cognome == null)
                t.Cognome = vecchio.Cognome;
            if (t.Telefono == null)
                t.Telefono = vecchio.Telefono;


            using (SqlConnection c = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "UPDATE Contatti SET " +
                                        "nome = @varNome, " +
                                        "cognome = @varCognome, " +
                                        "telefono = @varTelefono " +
                                        "WHERE rubricaID = @varId";

                cmd.Parameters.AddWithValue("@varNome", t.Nome);
                cmd.Parameters.AddWithValue("@varCognome", t.Cognome);
                cmd.Parameters.AddWithValue("@varTelefono", t.Telefono);
                cmd.Parameters.AddWithValue("@varId", t.Id);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}
