﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_01_TaskCorrezioneAmpliamento.Models
{
    class Contatto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Telefono { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Nome} - {Cognome} - {Telefono}";
        }
    }
}
