﻿using DB_01_TaskCorrezioneAmpliamento.DAL;
using DB_01_TaskCorrezioneAmpliamento.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_01_TaskCorrezioneAmpliamento.Controllers
{
    public class ContattoController
    {
        private static IConfiguration configuration;

        public ContattoController()
        {
            if(configuration == null)
            {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json", false, false);

                configuration = builder.Build();
            }
        }

        //TODO: Ricerca contatto
        public void ricercaContatti()
        {
            List<Contatto> elenco = new ContattoDAL(configuration).ReadAll();

            foreach(Contatto item in elenco)
                Console.WriteLine(item);


        }

        public void ricercaContattoPerId(int varId)
        {
            Contatto temp = new ContattoDAL(configuration).ReadById(varId);

            if (temp == null)
                Console.WriteLine("Non ho trovato ciò che cercavi!");
            else
                Console.WriteLine(temp);
        }


        //TODO: Inserisci contatto
        //TODO: Elimina contatto

        public void modificaContatto(int varId, string varNome, string varCognome, string varTelefono)
        {
            ContattoDAL contattoDAL = new ContattoDAL(configuration);

            Contatto nuovo = new Contatto();
            nuovo.Id = varId;
            nuovo.Cognome = varCognome;
            nuovo.Nome = varNome;
            nuovo.Telefono = varTelefono;

            if (contattoDAL.Update(nuovo))
            {
                Console.WriteLine(contattoDAL.ReadById(varId));
            }
            else
            {
                Console.WriteLine("Problema di update!");
            }
        }
    }
}
