﻿using DB_01_TaskCorrezioneAmpliamento.Controllers;
using System;

namespace DB_01_TaskCorrezioneAmpliamento
{
    class Program
    {
        /*
            * Creare un sistema di gestione di contatti, che memorizzi:
            * Nome
            * Cognome
            * Telefono (UNIVOCO)
            * 
            * - CRUD con DB
            * - Evitare l'inserimento di elementi già presenti in DB (evitare i metodi read generici)
        */

        static void Main(string[] args)
        {
            ContattoController gestore = new ContattoController();
            //gestore.ricercaContatti();

            //gestore.ricercaContattoPerId(2);

            gestore.modificaContatto(2, "Bastoncino", "Gombloddistico", "+39123456");
            
        }
    }
}
