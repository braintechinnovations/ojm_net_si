﻿using System;

namespace Lez02_02_ControlliComplessi
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Inserisci la provincia (2 digit)");
            //string provincia = Console.ReadLine();

            //if (provincia.Equals("BO"))
            //{
            //    Console.WriteLine("Sei a Bologna");
            //} else
            //{
            //    if (provincia.Equals("AQ"))
            //    {
            //        Console.WriteLine("Sei a L'Aquila");
            //    }
            //    else
            //    {
            //        if (provincia.Equals("MI"))
            //        {
            //            Console.WriteLine("Sei a Milano");
            //        }
            //        else
            //        {
            //            Console.WriteLine("Non so dove sei");
            //        }
            //    }
            //}

            //if (provincia.Equals("BO"))
            //    Console.WriteLine("Sei a Bologna");
            //else if (provincia.Equals("AQ"))
            //    Console.WriteLine("Sei a L'Aquila");
            //else if (provincia.Equals("MI"))
            //    Console.WriteLine("Sei a Milano");
            //else
            //    Console.WriteLine("Non so dove sei");

            //---------------------------------------------------

            //if (provincia.Equals("BO"))
            //    Console.WriteLine("Sei a Bologna");

            //if (provincia.Equals("AQ"))
            //    Console.WriteLine("Sei a L'Aquila");

            //if (provincia.Equals("MI"))
            //    Console.WriteLine("Sei a Milano");

            //---------------------------------------------------

            //switch (provincia)
            //{
            //    case "AQ":
            //        Console.WriteLine("Sei a L'Aquila");
            //        break;
            //    case "MI":
            //        Console.WriteLine("Sei a Milano");
            //        break;
            //    case "BO":
            //        Console.WriteLine("Sei a Bologna");
            //        break;
            //    default:
            //        Console.WriteLine("Non so dove sei");
            //        break;
            //}

            /*
             * * CON SWITCH-CASE
		         * Scrivere un piccolo programma che, dato in input il giorno 
		         * della settimana (in numero), vi restituisca il nome
		         * - 1 = Lunedì
		         * - 2 = Martedì
		         * ...
		         * 
		         * 
		         * NON RISCRIVENDO LO SWITCH-CASE ;)
		         * CHALLENGE... e se volessimo cambiare il sistema di conversione 
		         * dei giorni nello stile americano?
		         * - 1 = Domenica
		         * - 2 = Lunedì
		         * - 3 = Martedì
		         * ...
		         * 
		         * TIP: All'inizio chiedete se italiano o inglese
		         */

            //Soluzione esercizio 1
            //Console.WriteLine("Inserisci il giorno (numerico)");
            //int giorno = Convert.ToInt32(Console.ReadLine());

            //switch (giorno)
            //{
            //    case 1:
            //        Console.WriteLine("Lunedì");
            //        break;
            //    case 2:
            //        Console.WriteLine("Martedì");
            //        break;
            //    case 3:
            //        Console.WriteLine("Mercoledì");
            //        break;
            //    case 4:
            //        Console.WriteLine("Giovedì");
            //        break;
            //    case 5:
            //        Console.WriteLine("Venerdì");
            //        break;
            //    case 6:
            //        Console.WriteLine("Sabato");
            //        break;
            //    case 7:
            //        Console.WriteLine("Domenica");
            //        break;
            //    default:
            //        Console.WriteLine("Valore non consentito");
            //        break;
            //}

            //Soluzione esercizio 2
            Console.WriteLine("ITA (default) o ENG?");
            string local = Console.ReadLine();

            if (local.Equals("ITA") || local.Equals("ENG"))
            {
                Console.WriteLine("Inserisci il giorno (numerico)");
                int giorno = Convert.ToInt32(Console.ReadLine());

                if (local.Equals("ENG"))
                {
                    if (giorno == 1)
                        giorno = 7;
                    else
                        giorno--;
                }

                switch (giorno)
                {
                    case 1:
                        Console.WriteLine("Lunedì");
                        break;
                    case 2:
                        Console.WriteLine("Martedì");
                        break;
                    case 3:
                        Console.WriteLine("Mercoledì");
                        break;
                    case 4:
                        Console.WriteLine("Giovedì");
                        break;
                    case 5:
                        Console.WriteLine("Venerdì");
                        break;
                    case 6:
                        Console.WriteLine("Sabato");
                        break;
                    case 7:
                        Console.WriteLine("Domenica");
                        break;
                    default:
                        Console.WriteLine("Valore non consentito");
                        break;
                }
            }
            else
                Console.WriteLine("Errore, non hai selezionato la lingua giusta");
        }
    }
}
