﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VIEW_Lez06_ListView_Template.Classes;

namespace VIEW_Lez06_ListView_Template
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Persona> elenco = new List<Persona>();

            elenco.Add(new Persona() { Nome = "Giovanni", Cognome = "Pace", Email = "giovanni@test.com"});
            elenco.Add(new Persona() { Nome = "Mario", Cognome = "Rossi", Email = "mario@test.com"});
            elenco.Add(new Persona() { Nome = "Valeria", Cognome = "Verdi", Email = "valeria@test.com"});
            elenco.Add(new Persona() { Nome = "Marika", Cognome = "Mariko", Email = "marika@test.com"});

            elencoListaSemplice.ItemsSource = elenco;
            elencoListaComplesso.ItemsSource = elenco;
            elencoListaTabella.ItemsSource = elenco;
        }
    }
}
