﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_07_Poly.Classes
{
    public class Studente : Persona
    {
        public string Matricola { get; set; }

        public override string ToString()
        {
            return $"{Nome} {Cognome} - Matricola: {Matricola}";
        }
    }
}
