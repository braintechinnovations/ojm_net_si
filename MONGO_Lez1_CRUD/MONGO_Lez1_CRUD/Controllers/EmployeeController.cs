﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_Lez1_CRUD.DTO;
using MONGO_Lez1_CRUD.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez1_CRUD.Controllers
{
    [ApiController]
    [Route("api/employees")]
    public class EmployeeController : Controller
    {
        private IMongoCollection<Employee> impiegati;

        public EmployeeController(IConfiguration configurazione)
        {
            //Navigo nel file appsettings.json in modalità personalizzata
            string stringaConnessione  = configurazione.GetValue<string>("MongoDbSettings:MongoLocale");
            string database            = configurazione.GetValue<string>("MongoDbSettings:NomeDatabase");

            MongoClient client = new MongoClient(stringaConnessione);
            IMongoDatabase db = client.GetDatabase(database);

            impiegati = db.GetCollection<Employee>("Employees");
        }

        [HttpPost("insert")]
        public ActionResult<Employee> InserisciImpiegato(Employee emp)
        {

            if (ModelState.IsValid)
            {
                Employee temp = impiegati.Find(o => o.Matricola == emp.Matricola).FirstOrDefault();

                if(temp == null)
                    impiegati.InsertOne(emp);
            }
            else
            {   
            }

            return Ok(emp);
        }

        [HttpGet]
        public ActionResult<IEnumerable<EmployeeDTO>> ListaImpiegati()
        {
            List<Employee> elenco = impiegati.Find(FilterDefinition<Employee>.Empty).ToList();
            List<EmployeeDTO> elencoDTO = new List<EmployeeDTO>();

            foreach(var item in elenco)
            {
                elencoDTO.Add(new EmployeeDTO()
                {
                    Id = item.DocumentID.ToString(),
                    Nome = item.Nome,
                    Cogn = item.Cognome,
                    Tito = item.Titolo
                });
            }

            return Ok(elencoDTO);
        }

        [HttpGet("{idDocumento}")]
        public ActionResult<EmployeeDTO> RicercaPerId(string idDocumento)
        {
            ObjectId idTrasformato = new ObjectId(idDocumento);
            var filter = Builders<Employee>.Filter.Eq(e => e.DocumentID, idTrasformato);
            Employee risultato = impiegati.Find(filter).FirstOrDefault();

            return Ok(
                new EmployeeDTO()
                {
                    Id = risultato.DocumentID.ToString(),
                    Nome = risultato.Nome,
                    Cogn = risultato.Cognome,
                    Tito = risultato.Titolo
                }
            );
        }

        [HttpDelete("{idDocumento}")]
        public ActionResult EliminaPerId(string idDocumento)
        {
            ObjectId idTrasformato = new ObjectId(idDocumento);

            Employee temp = impiegati.Find(e => e.DocumentID == idTrasformato).FirstOrDefault();
            if (temp != null)
            {
                var risultato = impiegati.DeleteOne<Employee>(e => e.DocumentID == idTrasformato);

                if (risultato.IsAcknowledged && risultato.DeletedCount > 0)
                    return Ok(new { Status = "success" });
                else
                    return Ok(new { Status = "error" });
            }
            else
            {
                return Ok(new { Status = "error", Descrizione = "Non ho trovato il documento" });
            }
        }

        [HttpPut("{idDocumento}")]
        public ActionResult ModificaImpiegato(string idDocumento, Employee emp)
        {
            ObjectId idTrasformato = new ObjectId(idDocumento);

            Employee temp = impiegati.Find(e => e.DocumentID == idTrasformato).FirstOrDefault();
            if (temp != null)
            {
                temp.Nome = emp.Nome != null ? emp.Nome : temp.Nome;
                temp.Cognome = emp.Cognome != null ? emp.Cognome : temp.Cognome;
                temp.Titolo = emp.Titolo != null ? emp.Titolo : temp.Titolo;
                temp.Matricola = emp.Matricola != 0 ? emp.Matricola : temp.Matricola;
                temp.DataNascita = emp.DataNascita != null ? emp.DataNascita : temp.DataNascita;

                //TODO: Verifica che non esista un altro Employee con la stessa matricola!

                var filter = Builders<Employee>.Filter.Eq(e => e.DocumentID, idTrasformato);
                var risultato = impiegati.ReplaceOne(filter, temp);

                if(risultato.IsAcknowledged && risultato.ModifiedCount > 0)
                    return Ok(new { Status = "success" });
                else
                    return Ok(new { Status = "error" });
            }
            else
            {
                return Ok(new { Status = "error", Descrizione = "Non ho trovato il documento" });
            }
        }
    }
}
