﻿using Lez04_02_TaskEreditarieta.Classes;
using System;

namespace Lez04_02_TaskEreditarieta
{
    class Program
    {
        static void Main(string[] args)
        {
            //int a = 5;
            //string valoreStringa = a.ToString();
            //valoreStringa = Convert.ToString(a);

            //Persona giovanni = new Persona("Giovanni", "Pace");
            //Persona mario = new Persona("Mario", "Rossi");

            //Console.WriteLine(giovanni);
            //Console.WriteLine(mario);

            Studente giovanni = new Studente("Giovanni", "Pace");
            Studente mario = new Studente("Mario", "Rossi");

            Console.WriteLine(giovanni);
            Console.WriteLine(mario);

            //Studente.ContaStudenti = -8;

            Studente mirko = new Studente("Mirko", "Marki");
            Console.WriteLine(mirko);


        }
    }
}
