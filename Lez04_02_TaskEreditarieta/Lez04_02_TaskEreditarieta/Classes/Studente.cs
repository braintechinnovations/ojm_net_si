﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_02_TaskEreditarieta.Classes
{
    class Studente : Persona
    {
        private static int contaStudenti = 0;

        public static int ContaStudenti
        {
            get { return contaStudenti = 0; }
            //set { contaStudenti = value; }
        }

        public int Matricola { get; set; }

        public Studente(string varNome, string varCognome)
        {
            Nome = varNome;
            Cognome = varCognome;

            contaStudenti++;

            Matricola = contaStudenti;
        }

        public override string ToString()
        {
            return $"Studente: {Nome}, {Cognome} - Matricola: {Matricola}";
        }
    }
}
