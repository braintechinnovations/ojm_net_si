﻿using Lez04_07_TaskClassi.Classes;
using System;

namespace Lez04_07_TaskClassi
{
    class Program
    {
        static void Main(string[] args)
        {

            /**
			 * Creare un sistema in grado di immagazinare i dati relativi ad una persona.
			 * Inoltre, sarà necessario immagazinare, all'interno di una persona, i dati 
			 * relativi a:
			 * - Codice Fiscale
			 * |- CODICE
			 * |_ Data di scadenza
			 * 
			 * - Carta di Identita: 
			 * |- CODICE
			 * |- Data di Emissione
			 * |- Data di Scadenza
			 * |_ Emissione (comune || zecca dello stato)
			 */

            Persona giorgio = new Persona();
            giorgio.Nome = "Giorgio";
            giorgio.Cognome = "Prova";
            giorgio.CodFis = new CodiceFiscale()
            {
                Codice = "GRGPRV",
                DataScadenza = new DateTime(2022, 03, 10)
            };

            giorgio.CarIde = new CartaIdentita()
            {
                Codice = "123456",
                DataScadenza = new DateTime(2022, 03, 10),
                DataEmissione = new DateTime(2012, 03, 10),
                Emissione = "Zecca"
            };

            Console.WriteLine(giorgio);

            //---------------------------------------------------------

            //DateTime oggi = new DateTime(2022, 03, 10);

            ////Console.WriteLine(oggi.Year);
            ////Console.WriteLine(oggi.Month);
            ////Console.WriteLine(oggi.Day);
            ////Console.WriteLine(oggi.DayOfWeek);
            ////Console.WriteLine(oggi.DayOfYear);

            ////Console.WriteLine(oggi);

            ////oggi = oggi.AddDays(-54);

            ////Console.WriteLine(oggi);

            //Console.WriteLine(oggi.ToString("yyyy-MM-dd"));

        }
	}
}
