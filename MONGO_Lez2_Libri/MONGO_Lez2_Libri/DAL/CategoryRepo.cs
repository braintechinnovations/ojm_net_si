﻿using MONGO_Lez2_Libri.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {
        private IMongoCollection<Category> categorie;

        public CategoryRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if(categorie == null)
                categorie = db.GetCollection<Category>("Categories");
        }

        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAll()
        {
            return categorie.Find(FilterDefinition<Category>.Empty).ToList();
        }

        public Category GetById(ObjectId varId)
        {
            return categorie.Find(d => d.DocumentID == varId).FirstOrDefault();
        }

        public bool Insert(Category t)
        {
            Category temp = categorie.Find(d => d.Codice == t.Codice).FirstOrDefault();
            if (temp == null)
            {
                categorie.InsertOne(t);
                if(t.DocumentID != null)
                    return true;
            }

            return false;
        }

        public bool Update(Category t)
        {
            throw new NotImplementedException();
        }

        public Category FindByTitle(string varTitolo)
        {
            return categorie.Find(d => d.Titolo == varTitolo).FirstOrDefault();
        }

    }
}
