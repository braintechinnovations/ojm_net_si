﻿using MONGO_Lez2_Libri.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.DAL
{
    public class BookRepo : InterfaceRepo<Book>
    {
        private IMongoCollection<Book> libri;

        //Rendo disponibili le stringhe per l'utilizzo futuro!
        private string strConne;
        private string strDatab;

        public BookRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if (libri == null)
                libri = db.GetCollection<Book>("Books");

            strConne = strConnessione;
            strDatab = strDatabase;
        }

        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Book> GetAll()
        {
            List<Book> elenco = libri.Find(FilterDefinition<Book>.Empty).ToList();

            CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
            foreach (var item in elenco)
            {
                item.InfoCategoria = tempRepo.GetById(item.Category);
            }

            return elenco;
        }

        public Book GetById(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Book t)
        {
            Book temp = libri.Find(d => d.Codice == t.Codice).FirstOrDefault();
            if(temp == null)
            {
                //Caso in cui la REST mi invia il Nome Categoria
                CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
                Category tempCate = tempRepo.FindByTitle(t.Categoria);

                if(tempCate != null)
                {
                    t.Category = tempCate.DocumentID;
                    libri.InsertOne(t);
                    if (t.DocumentID != null)
                        return true;
                }
                    
            }

            return false;
        }

        public bool Update(Book t)
        {
            throw new NotImplementedException();
        }
    }
}
