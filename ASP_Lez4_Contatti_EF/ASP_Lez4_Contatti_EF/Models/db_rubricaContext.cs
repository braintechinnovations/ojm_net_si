﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ASP_Lez4_Contatti_EF.Models
{
    public partial class db_rubricaContext : DbContext
    {
        public db_rubricaContext()
        {
        }

        public db_rubricaContext(DbContextOptions<db_rubricaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contatti> Contattis { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-6IND805\\SQLEXPRESS;Database=db_rubrica;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Contatti>(entity =>
            {
                entity.HasKey(e => e.RubricaId)
                    .HasName("PK__Contatti__A6706BF0159A1688");

                entity.ToTable("Contatti");

                entity.HasIndex(e => e.Telefono, "UQ__Contatti__2A16D9453174E773")
                    .IsUnique();

                entity.Property(e => e.RubricaId).HasColumnName("rubricaID");

                entity.Property(e => e.Cognome)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("cognome");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("nome");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("telefono");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
