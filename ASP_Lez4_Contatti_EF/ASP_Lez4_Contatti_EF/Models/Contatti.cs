﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ASP_Lez4_Contatti_EF.Models
{
    public partial class Contatti
    {
        public int RubricaId { get; set; }
        public string Nome { get; set; }
        public string Telefono { get; set; }
        public string Cognome { get; set; }
    }
}
