﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_04_TaskCasa.Classes
{
    public class Stanza
    {
        public string Nome { get; set; }

        public List<Oggetto> Elenco { get; set; }

        public Stanza()
        {
            Elenco = new List<Oggetto>();
        }
    }
}
