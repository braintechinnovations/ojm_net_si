﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EF_Lez01_OneToMany.Models
{
    public partial class Persona
    {
        public Persona()
        {
            CartaFedelta = new HashSet<CartaFedeltum>();
        }

        public int PersonaId { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Indirizzo { get; set; }
        public string Email { get; set; }

        public virtual ICollection<CartaFedeltum> CartaFedelta { get; set; }
    }
}
