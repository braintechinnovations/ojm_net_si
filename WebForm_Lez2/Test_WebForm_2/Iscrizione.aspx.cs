﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Test_WebForm_2.Models;

namespace Test_WebForm_2
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblRisultato.Text = (string)Session["sesNome"];

            if(Session["utente"] != null)
            {
                Utente temp = (Utente)Session["utente"];

                lblUsername.Text = temp.Username;
                lblDataLogin.Text = temp.DataLogin.ToString();
            }
            else
            {
                Session["errore"] = "Devi prima effettuare il login per visualizzare la pagina";
                Response.Redirect("Default.aspx");
            }
            

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }
}