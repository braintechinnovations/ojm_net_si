﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Test_WebForm_2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col">

            <div class="form-group">
                <label for="tbUser">Username</label>
                <asp:TextBox ID="tbUser" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="tbPass">Password</label>
                <asp:TextBox ID="tbPass" class="form-control" runat="server"></asp:TextBox>
            </div>


            <% if (Errore != null && Errore != "")
                { %>
                    <div class="alert alert-danger" role="alert">
                        <%: Errore %>
                    </div>
            <% } %>

            <asp:Button ID="btnInvia" class="btn btn-primary btn-block" runat="server" Text="Effettua il Login" OnClick="btnInvia_Click"/>

        </div>
    </div>

</asp:Content>
