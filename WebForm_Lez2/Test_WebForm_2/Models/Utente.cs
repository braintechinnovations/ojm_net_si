﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_WebForm_2.Models
{
    public class Utente
    {
        public string Username { get; set; }

        public DateTime DataLogin { get; set; }

        public Utente()
        {
            DataLogin = DateTime.Now;
        }

    }
}