﻿using System;

namespace Lez02_05_ContenitoriSemplici
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] elencoNumeri = { 1, 15, 8, 98, 12 };

            //Console.WriteLine(elencoNumeri[2]);     //Accedo alla posizione 2

            //Console.WriteLine(elencoNumeri.Length); //Lunghezza Array

            //Console.WriteLine(elencoNumeri[elencoNumeri.Length - 1]);

            //----------------------------------------------

            //string[] elencoAlimenti = new string[5];

            //elencoAlimenti[0] = "Patate";
            //elencoAlimenti[1] = "Pasta";
            //elencoAlimenti[2] = "Gnocchi";

            //---------------------- Scansione con Loop While

            //string[] elencoAlimenti = {
            //    "patate" ,
            //    "pasta" ,
            //    "gnocchi" , 
            //    //"biscotti" ,
            //    //"broccoli",
            //    //"panini"
            //};

            //int i = 0;

            //while (i < elencoAlimenti.Length)
            //{
            //    Console.WriteLine(elencoAlimenti[i]);

            //    i++;
            //}

            //---------------------- Scansione con Loop Do While (pericoloso con Array)

            //string[] elencoAlimenti = { };

            //int i = 0;

            //do
            //{
            //    Console.WriteLine(elencoAlimenti[i]);

            //    i++;
            //} while (i < elencoAlimenti.Length);

            //---------------------- Scansione con Loop For

            //string[] elencoalimenti = {
            //    "patate" ,
            //    "pasta" ,
            //    "gnocchi" , 
            //    //"biscotti" ,
            //    //"broccoli",
            //    //"panini"
            //};

            //for (int i = 0; i<elencoalimenti.Length; i++)
            //{
            //    Console.WriteLine($"Alimento: {elencoalimenti[i]} alla posizione: {i + 1}");
            //}

            //---------------------- Ricerca computata
            //string[] animali = { "Gatto", "Cane", "Elefante", "Scimmia", "Scoiattolo" };

            //bool trovato = false;

            //Console.WriteLine("Digita l'animale:");
            //string ricerca = Console.ReadLine();

            //for(int i=0; i<animali.Length; i++)
            //{
            //    string temp = animali[i];

            //    if (temp.Equals(ricerca))
            //    {
            //        trovato = true;
            //    }
            //}

            //Console.WriteLine(trovato ? "L'ho trovato" : "Non l'ho trovato");

            //---------------------------- Ciclo For Each

            //string[] elencoAlunni = { "Giovanni", "Mario", "Antonio", "Marika", "Valeria" };

            ////Array.Sort(elencoAlunni);

            ////elencoAlunni[5] = "Mirko";  //!!!!! NON PERMESSO!

            //foreach(string alunno in elencoAlunni)
            //{
            //    Console.WriteLine(alunno);
            //}


            //---------------------------- Uguaglianza tra stringhe

            //string varUno = "Giovanni ";
            //string varDue = "GioVanni";

            //if (varUno.ToLower().Trim().Equals(varDue.ToLower().Trim()))
            //{
            //    Console.WriteLine("Uguali");
            //}
            //else
            //{
            //    Console.WriteLine("Diverse");
            //}



        }
    }
}
