﻿using Lez05_06_RefactorTask.Classes;
using System;
using System.Collections.Generic;

namespace Lez05_06_RefactorTask
{
    class Program
    {
        static void Main(string[] args)
        {

            bool inserimentoAbilitato = true;
            GestoreStudente gestore = new GestoreStudente();

            while (inserimentoAbilitato)
            {
                Console.WriteLine(
                        "1.inserimento nuovo studente\n" +
                        "2.aggiornamento dati studente\n" +
                        "3.eliminazione studente\n" +
                        "4.visualizzazione dettaglio scheda studente\n" +
                        "5.visualizzazione lista studenti\n" +
                        "6.verifica risultati studente\n" +
                        "7.Uscita dall'applicazione\n"
                );
                string input = Console.ReadLine();

                string nominativo, matricola, citta, cfu;

                switch (input)
                {
                    case "1":             //Inserimento nuovo studente
                        Console.WriteLine("Nominativo:");
                        nominativo = Console.ReadLine();

                        Console.WriteLine("Matricola:");
                        matricola = Console.ReadLine();

                        Console.WriteLine("Città:");
                        citta = Console.ReadLine();

                        Console.WriteLine("Cfu:");
                        cfu = Console.ReadLine();

                        if (gestore.inserisciStudente(nominativo, matricola, citta, cfu))
                            Console.WriteLine("Tutto ok!");
                        else
                            Console.WriteLine("Errore di inseirmento");
                        break;
                    case "2":
                        Console.WriteLine("Matricola:");
                        matricola = Console.ReadLine();

                        Console.WriteLine("Nominativo:");
                        nominativo = Console.ReadLine();


                        Console.WriteLine("Città:");
                        citta = Console.ReadLine();

                        Console.WriteLine("Cfu:");
                        cfu = Console.ReadLine();

                        if (gestore.modificaStudente(matricola, nominativo, citta, cfu))
                            Console.WriteLine("OK!");
                        else
                            Console.WriteLine("Errore");

                        break;
                    case "3":
                        Console.WriteLine("Matricola:");
                        matricola = Console.ReadLine();

                        if (gestore.eliminaStudente(matricola))
                            Console.WriteLine("Eliminazione avvenuta con successo!");
                        else
                            Console.WriteLine("Errore di eliminazione");
                        break;
                    case "4":
                        Console.WriteLine("Matricola:");
                        matricola = Console.ReadLine();

                        Studente temp = gestore.cercaStudente(matricola);
                        if(temp == null)
                            Console.WriteLine("Non l'ho trovato!");
                        else
                            Console.WriteLine(temp);
                        break;
                    case "5":
                        gestore.listaStudenti();
                        break;
                    case "6":
                        Console.WriteLine("Matricola:");
                        matricola = Console.ReadLine();

                        Console.WriteLine(gestore.verificaRisultati(matricola));
                        break;
                    case "7":
                        inserimentoAbilitato = !inserimentoAbilitato;
                        break;
                    default:
                        Console.WriteLine("Comando non trovato! ;(");
                        break;
                }
            }

        }
    }
}
