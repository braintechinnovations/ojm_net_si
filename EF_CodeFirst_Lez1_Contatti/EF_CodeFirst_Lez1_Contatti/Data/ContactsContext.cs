﻿using EF_CodeFirst_Lez1_Contatti.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Lez1_Contatti.Data
{
    public class ContactsContext : DbContext
    {
        public ContactsContext(DbContextOptions<ContactsContext> opt) : base(opt)
        {

        }

        public DbSet<Contact> Contacts { get; set; }
    }
}
