﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Lez04_08_ContenitoriSemplici
{
    class Studente
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //ArrayList elenco = new ArrayList();

            //elenco.Add("BMW");
            //elenco.Add("Lamborghini");
            //elenco.Add("Maserati");
            //elenco.Add("FIAT");

            //for(int i=0; i<elenco.Count; i++)
            //{
            //    Console.WriteLine(elenco[i]);
            //}

            //Console.WriteLine();

            //elenco.Add("Koningsen");

            //for (int i = 0; i < elenco.Count; i++)
            //{
            //    Console.WriteLine(elenco[i]);
            //}

            //-----------------------------------------------

            Studente giovanni = new Studente();

            ArrayList elenco = new ArrayList();

            elenco.Add("Mortadella");
            elenco.Add(5);
            elenco.Add(giovanni);

            //for (int i = 0; i < elenco.Count; i++)
            //{
            //    if (elenco[i] == 5)
            //        Console.WriteLine("Trovato");
            //}


            List<string> elenco2 = new List<string>();

            elenco2.Add("Mortadella");
            elenco2.Add(5);
        }
    }
}
