-- Uno a molti

CREATE TABLE Persona(
	personaID INTEGER PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(250) NOT NULL,
	cognome VARCHAR(250) NOT NULL,
	indirizzo VARCHAR(750) DEFAULT 'N.D.',
	email VARCHAR(250) UNIQUE NOT NULL
);

CREATE TABLE CartaFedelta(
	cartaID INTEGER PRIMARY KEY IDENTITY(1,1),
	codiceCarta VARCHAR(25) NOT NULL UNIQUE,
	negozio VARCHAR(50),
	personaRIF INTEGER NOT NULL,
	FOREIGN KEY (personaRIF) REFERENCES Persona(personaID) ON DELETE CASCADE
);

INSERT INTO Persona (nome, cognome, email) VALUES
('Giovanni', 'Pace', 'gio@ciao.com'),
('Mario', 'Rossi', 'mar@ciao.com'),
('Valeria', 'Verdi', 'val@ciao.com');

INSERT INTO CartaFedelta (codiceCarta, negozio, personaRIF) VALUES
('123456', 'CONAD',	1),
('123457', 'CONAD',	2),
('98COOP', 'COOP', 1),
('99COOP', 'COOP', 3);

--INSERT INTO Persona (nome, cognome, email) VALUES
--('Mirko', 'Marchi', 'mir@ciao.com');

--SELECT * FROM Persona;
--SELECT * FROM CartaFedelta;

--SELECT Persona.nome, Persona.cognome, CartaFedelta.codiceCarta, CartaFedelta.negozio FROM Persona
--	JOIN CartaFedelta ON Persona.personaID = CartaFedelta.personaRIF;


SELECT * FROM Persona
	LEFT JOIN CartaFedelta ON Persona.personaID = CartaFedelta.personaRIF;

DELETE FROM Persona WHERE personaID = 1;