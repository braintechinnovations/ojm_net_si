﻿using System;

namespace Lez02_09_MetodiInnestati
{
    class Program
    {
        static double EffettuaDivisione(double numUno, double numDue)
        {
            if (numDue != 0)
                return effettuaOperazione(numUno, numDue);
            else
                return -1;
        }

        static double effettuaOperazione(double varUno, double varDue)
        {
            return varUno / varDue;
        }

        static void Main(string[] args)
        {
            double a = 5.6d;
            double b = 0d;

            double risultato = EffettuaDivisione(a, b);
            if (risultato == -1)
            {
                Console.WriteLine("Impossibile dividere per zero");
            }
            else
            {
                Console.WriteLine($"Il risultato è: {risultato}");
            }
         
            /*
             * Creare software che incorpori un metodo che verifichi la temperatura in ingresso di un locale.
             * La verifica deve essere sequenziale quindi dopo aver inserito una temperatura ed aver
             * ottenuto il risultato posso continuare con altre nuove verifiche. 
             * Terminare il programma all'inseirmento di una apposita stringa.
             */
        }
    }
}
