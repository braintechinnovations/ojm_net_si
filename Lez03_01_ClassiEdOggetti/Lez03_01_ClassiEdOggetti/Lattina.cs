﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_01_ClassiEdOggetti
{
    class Lattina
    {
        public string marca;
        public string contenuto;
        public double diametro;
        public double altezza;

        public double volumeLattina()
        {
            double raggio = this.diametro / 2;
            double areaCerchio = Math.PI * (raggio * raggio);
            double volume = areaCerchio * this.altezza;

            return volume;
        }

        public void stampaDettaglio()
        {
            //double volume = volumeLattina();
            //Console.WriteLine($"{this.marca} - {this.contenuto} - VOLUME: {volume}");

            Console.WriteLine($"{this.marca} - {this.contenuto} - VOLUME: {this.volumeLattina()}");
        }
    }
}
