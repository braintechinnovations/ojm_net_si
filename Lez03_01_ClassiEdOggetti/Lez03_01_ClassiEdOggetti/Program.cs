﻿using System;

namespace Lez03_01_ClassiEdOggetti
{
    class Program
    {
        static void Main(string[] args)
        {

            Bottiglia acqua = new Bottiglia();
            acqua.contenuto = "Acqua naturale";
            acqua.marca = "Santacroce";
            acqua.materiale = "PET";
            acqua.volume = 12.58d;

            Bottiglia aranciata = new Bottiglia();
            aranciata.contenuto = "Aranciata";
            aranciata.marca = "San Pellegrino";
            aranciata.materiale = "ALU";
            aranciata.volume = 2.5d;

            Lattina coca = new Lattina();
            coca.contenuto = "Coca";
            coca.marca = "Blues";
            coca.altezza = 8.0d;
            coca.diametro = 3.0d;

            //double volumeCoca = coca.volumeLattina();
            //Console.WriteLine(volumeCoca);

            coca.stampaDettaglio();

        }
    }
}
