﻿using ASP_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Data
{
    public class MockContatti : IContattiRepo
    {
        public IEnumerable<Contatto> GetContatti()
        {
            var elenco = new List<Contatto>
            {
                new Contatto() { Id = 1, Nome = "Giovanni", Cognome = "Pace", CodFis = "PCAGNN"},
                new Contatto() { Id = 2, Nome = "Mario", Cognome = "Rossi", CodFis = "MRRRSS"},
                new Contatto() { Id = 3, Nome = "Valeria", Cognome = "Verdi", CodFis = "VLRVRD"},
                new Contatto() { Id = 4, Nome = "Marika", Cognome = "Mariko", CodFis = "MRKMRK"},
            };

            return elenco;
        }

        public Contatto GetContattoById(int varId)
        {
            var persona = new Contatto() { Id = 3, Nome = "Valeria", Cognome = "Verdi", CodFis = "VLRVRD" };
            return persona;
        }
    }
}
