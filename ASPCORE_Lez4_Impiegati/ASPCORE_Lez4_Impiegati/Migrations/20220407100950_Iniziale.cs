﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPCORE_Lez4_Impiegati.Migrations
{
    public partial class Iniziale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Citta",
                columns: table => new
                {
                    CittaID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeCitta = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Citta", x => x.CittaID);
                });

            migrationBuilder.CreateTable(
                name: "Impiegati",
                columns: table => new
                {
                    ImpiegatoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Cognome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Titolo = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Citta = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Impiegati", x => x.ImpiegatoID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Citta");

            migrationBuilder.DropTable(
                name: "Impiegati");
        }
    }
}
