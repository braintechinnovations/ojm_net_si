﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Models
{
    [Table("Impiegati")]
    public class Impiegato
    {
        [Column("ImpiegatoID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Codice { get; set; }

        [Required(ErrorMessage = "Il campo Nome è obbligatorio")]
        [MinLength(3, ErrorMessage = "Il campo Nome è troppo corto")]
        [StringLength(255, ErrorMessage = "Il campo Nome è troppo lungo")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Il campo Cognome è obbligatorio")]
        [MinLength(3, ErrorMessage = "Il campo Cognome è troppo corto")]
        [StringLength(255, ErrorMessage = "Il campo Cognome è troppo lungo")]
        public string Cognome { get; set; }

        [Required(ErrorMessage = "Il campo Titolo è obbligatorio")]
        [StringLength(255, ErrorMessage = "Il campo Titolo è troppo lungo")]
        public string Titolo { get; set; }

        [Required(ErrorMessage = "Il campo Città di residenza è obbligatorio")]
        [StringLength(255, ErrorMessage = "Il campo Città di residenza è troppo lungo")]
        public string Citta { get; set; }

        public Impiegato()
        {
            Codice = Guid.NewGuid().ToString();
        }

    }
}
