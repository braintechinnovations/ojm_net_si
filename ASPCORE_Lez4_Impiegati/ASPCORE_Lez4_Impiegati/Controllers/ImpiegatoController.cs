﻿using ASPCORE_Lez4_Impiegati.Data;
using ASPCORE_Lez4_Impiegati.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Controllers
{
    public class ImpiegatoController : Controller
    {
        private readonly InterfaceRepo<Impiegato> _repositoryImp;
        private readonly InterfaceRepo<Citta> _repositoryCit;

        public ImpiegatoController(InterfaceRepo<Impiegato> repImp, InterfaceRepo<Citta> repCit)
        {
            _repositoryImp = repImp;
            _repositoryCit = repCit;
        }


        public IActionResult Lista()
        {
            List<Impiegato> elenco = (List<Impiegato>)_repositoryImp.GetAll();

            ViewBag.Title = "Lista impiegati";

            return View(elenco);
        }

        private List<SelectListItem> PopolaCitta()
        {
            List<Citta> cittadine = (List<Citta>)_repositoryCit.GetAll();

            List<SelectListItem> selectBox = new List<SelectListItem>();

            // <option value="Value">Text</option>

            foreach (var item in cittadine)
            {
                selectBox.Add(
                    new SelectListItem()
                    {
                        Text = item.NomeCitta,
                        Value = item.NomeCitta
                    });
            }

            return selectBox;
        }

        public IActionResult Inserisci()
        {
            ViewBag.Citta = PopolaCitta();

            return View();
        }

        [HttpPost]
        public IActionResult Inserisci(Impiegato objImp)
        {
            ViewBag.Citta = PopolaCitta();

            if (ModelState.IsValid)
            {
                _repositoryImp.Insert(objImp);
                return Redirect("/");
            }
            else
            {

                return View(objImp);
            }

        }

        public IActionResult Dettaglio(string varCodice)
        {

            ViewBag.Citta = PopolaCitta();

            Impiegato temp = _repositoryImp.GetFromCodice(varCodice);

            if (temp == null)
                return Redirect("Impiegato/Errore");

            return View(temp);
        }

        [HttpPost]
        public IActionResult Modifica(Impiegato objImp)
        {

            //TODO...
            return View();
        }

        [HttpDelete]
        public ActionResult Elimina(string varCodice)
        {
            //TODO...
            return Ok(new { Status = "success" });
        }
    }
}
