﻿using ASPCORE_Lez4_Impiegati.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Data
{
    public class CittaRepo : InterfaceRepo<Citta>
    {
        private readonly AppDbContext _context;

        public CittaRepo(AppDbContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Citta> GetAll()
        {
            return _context.ElencoCitta.ToList();
        }

        public Citta GetById(int varId)
        {
            throw new NotImplementedException();
        }

        public Citta GetFromCodice(string codice)
        {
            throw new NotImplementedException();
        }

        public Citta Insert(Citta t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Citta t)
        {
            throw new NotImplementedException();
        }
    }
}
