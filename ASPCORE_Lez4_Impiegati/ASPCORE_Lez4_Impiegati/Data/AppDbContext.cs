﻿using ASPCORE_Lez4_Impiegati.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Impiegato> ElencoImpiegati { get; set; }
        public DbSet<Citta> ElencoCitta { get; set; }
    }
}
