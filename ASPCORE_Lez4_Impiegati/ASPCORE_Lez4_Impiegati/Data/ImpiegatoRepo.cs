﻿using ASPCORE_Lez4_Impiegati.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Data
{
    public class ImpiegatoRepo : InterfaceRepo<Impiegato>
    {
        private readonly AppDbContext _context;

        public ImpiegatoRepo(AppDbContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Impiegato> GetAll()
        {
            return _context.ElencoImpiegati.ToList();
        }

        public Impiegato GetById(int varId)
        {
            throw new NotImplementedException();
        }

        public Impiegato Insert(Impiegato t)
        {
            _context.ElencoImpiegati.Add(t);
            _context.SaveChanges();

            return t;
        }

        public bool Update(Impiegato t)
        {
            throw new NotImplementedException();
        }

        public Impiegato GetFromCodice(string codice)
        {
            return _context.ElencoImpiegati.Where(o => o.Codice == codice).FirstOrDefault();
        }
    }
}
