﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Test_WebForm_1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col">

            <div class="form-group">
                <label for="varNome">Nome</label>
                <asp:TextBox ID="varNome" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="varNome">Cognome</label>
                <asp:TextBox ID="varCognome" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="varNome">Email</label>
                <asp:TextBox ID="varEmail" class="form-control" runat="server"></asp:TextBox>
            </div>

            <asp:Button ID="btnUno" class="btn btn-primary btn-block" runat="server" Text="Iscrivimi con PostBack"/>
            
            <asp:Button ID="btnDue" class="btn btn-primary btn-block" runat="server" Text="Iscrivimi con Metodo" OnClick="btnDue_Click"/>

            <hr />


            <h1>Risultato</h1>
            <asp:Label ID="lblRisultato" runat="server" Text="Non ancora popolato"></asp:Label>

        </div>
    </div>

</asp:Content>
