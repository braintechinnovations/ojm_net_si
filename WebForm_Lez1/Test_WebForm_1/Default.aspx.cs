﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Test_WebForm_1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //PostBack
            //lblRisultato.Text = $"Nome: {varNome.Text} Cognome: {varCognome.Text} Email: {varEmail.Text}";
            lblRisultato.Text = "";
        }

        protected void btnDue_Click(object sender, EventArgs e)
        {
            lblRisultato.Text = $"Nome: {varNome.Text} Cognome: {varCognome.Text} Email: {varEmail.Text}";
        }
    }
}