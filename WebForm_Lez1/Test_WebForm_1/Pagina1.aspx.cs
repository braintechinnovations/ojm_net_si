﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Test_WebForm_1
{
    public partial class Pagina1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnInvia_Click(object sender, EventArgs e)
        {
            string nome = varNome.Text;
            string cognome = varCognome.Text;

            string queryString = $"?nome={nome}&cognome={cognome}";

            Response.Redirect("Pagina2.aspx" + queryString);
        }
    }
}