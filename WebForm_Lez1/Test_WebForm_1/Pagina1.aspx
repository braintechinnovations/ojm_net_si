﻿<%@ Page Title="Pagina Uno" Language="C#" AutoEventWireup="true" CodeBehind="Pagina1.aspx.cs" Inherits="Test_WebForm_1.Pagina1" MasterPageFile="~/Site.Master" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
<%--<% 
    string nome = "Giovanni";

    string saluta()
    {
        return "Ciao Giovanni";
    }
%>

    <%: nome %>
    <%: Title %>
    <%: saluta() %>--%>

    <div class="row">
        <div class="col">

            <div class="form-group">
                <label for="varNome">Nome</label>
                <asp:TextBox ID="varNome" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="varNome">Cognome</label>
                <asp:TextBox ID="varCognome" class="form-control" runat="server"></asp:TextBox>
            </div>

            <asp:Button ID="btnInvia" class="btn btn-primary btn-block" runat="server" Text="Iscrivimi con Metodo" OnClick="btnInvia_Click"/>

        </div>
    </div>


</asp:Content>