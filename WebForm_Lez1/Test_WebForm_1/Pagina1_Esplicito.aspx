﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagina1_Esplicito.aspx.cs" Inherits="Test_WebForm_1.Pagina1_Esplicito" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" method="post" action="Pagina2.aspx" runat="server">
        <div class="form-group">
                    <label for="varNome">Nome</label>
                    <asp:TextBox ID="nome" class="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="varNome">Cognome</label>
            <asp:TextBox ID="cognome" class="form-control" runat="server"></asp:TextBox>
        </div>

        <asp:Button ID="btnInvia" class="btn btn-primary btn-block" runat="server" Text="Iscrivimi con Metodo"/>
    </form>
</body>
</html>
