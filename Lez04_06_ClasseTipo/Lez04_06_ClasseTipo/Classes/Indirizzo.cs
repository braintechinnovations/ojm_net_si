﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_06_ClasseTipo.Classes
{
    public class Indirizzo
    {
        public string Via { get; set; }
        public string Civico { get; set; }
        public string Cap { get; set; }
        public string Citta { get; set; }
        public string Provincia { get; set; }

        public override string ToString()
        {
            return $"{Via}, {Civico} - CAP: {Cap} - {Citta}({Provincia})";
        }
    }
}
