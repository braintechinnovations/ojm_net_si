﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_06_ClasseTipo.Classes
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodFis { get; set; }

        private Indirizzo spedizione;

        public Indirizzo IndirizzoSpedizione
        {
            get { return spedizione; }
            set { spedizione = value; }
        }

        public Indirizzo IndirizzoFatturazione { get; set; }

        public override string ToString()
        {
            return $"{Nome}, {Cognome} - Cod.Fis: {CodFis}\n" +
                $"Indirizzo Spedizione: {IndirizzoSpedizione}\n" +
                $"Indirizzo Fatturaizone: {IndirizzoFatturazione}";
        }

    }
}
