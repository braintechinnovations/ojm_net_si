﻿using EF_CodeFirst_Task_Oggetti.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> opzioni) : base(opzioni)
        {

        }

        //Elenco dei modelli che mapperò con EF

        public DbSet<Oggetto> ElencoOggetti { get; set; }
    }
}

