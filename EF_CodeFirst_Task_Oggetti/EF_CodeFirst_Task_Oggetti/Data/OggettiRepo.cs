﻿using EF_CodeFirst_Task_Oggetti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Data
{
    public class OggettiRepo : IRepo<Oggetto>
    {
        private readonly DatabaseContext _context;

        public OggettiRepo(DatabaseContext con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            try
            {
                Oggetto temp = _context.ElencoOggetti.Where(o => o.Id == varId).First();
                _context.ElencoOggetti.Remove(temp);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<Oggetto> GetAll()
        {
            return _context.ElencoOggetti.ToList();
        }

        public Oggetto GetById(int varId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Oggetto t)
        {
            try
            {
                _context.ElencoOggetti.Add(t);
                _context.SaveChanges();
                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update(Oggetto t)
        {
            try
            {
                _context.ElencoOggetti.Update(t);
                _context.SaveChanges();

                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }

        public Oggetto FindByCode(string varCodice) {

            return _context.ElencoOggetti
                .Where(o => o.Codice.ToLower().Equals(varCodice.ToLower()))
                .FirstOrDefault();
        }
    }
}
