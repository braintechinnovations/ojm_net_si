﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Data
{
    public interface IRepo<T>
    {
        bool Insert(T t);

        IEnumerable<T> GetAll();

        T GetById(int varId);

        bool Update(T t);

        bool Delete(int varId);

        T FindByCode(string varCodice);


    }
}
