﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Models
{
    [Table("Deposito")]
    [Index(nameof(Codice), IsUnique = true)]
    public class Oggetto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Nome { get; set; }

        [MaxLength(500)]
        public string Descrizione { get; set; }

        [Required]
        [MaxLength(25)]
        public string Codice { get; set; }

        [Required]
        public int Quantita { get; set; }
    }
}
