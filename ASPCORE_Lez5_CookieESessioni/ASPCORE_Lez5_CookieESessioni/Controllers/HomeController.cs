﻿using ASPCORE_Lez5_CookieESessioni.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez5_CookieESessioni.Controllers
{
    public class HomeController : Controller
    {
        //public IActionResult Index()
        //{
        //    ViewBag.Title = "Home Page ;)";

        //    HttpContext.Response.Cookies.Append("linguaggio", "ITA");
        //    HttpContext.Response.Cookies.Append("dimensionefont", "22");

        //    return View();
        //}

        //public IActionResult Verifica()
        //{
        //    ViewBag.Linguaggio = HttpContext.Request.Cookies["linguaggio"];
        //    ViewBag.Font = HttpContext.Request.Cookies["dimensionefont"];

        //    return View();
        //}

        ////COOKIE CON REST

        //[HttpGet]
        //public ActionResult GimmeCookies()
        //{
        //    HttpContext.Response.Cookies.Append("linguaggio", "ITA");
        //    HttpContext.Response.Cookies.Append("dimensionefont", "22");

        //    return Ok(new { Status = "success" });
        //}


        //[HttpGet]
        //public ActionResult CheckCookies()
        //{
        //    string linguaggio = HttpContext.Request.Cookies["linguaggio"];
        //    string font = HttpContext.Request.Cookies["dimensionefont"];

        //    return Ok(new { 
        //        Status = "success",
        //        Linguaggio = linguaggio,
        //        DimFont = font
        //    });
        //}

        //public ActionResult GimmeCookieObject()
        //{
        //    Persona giovanni = new Persona()
        //    {
        //        Cognome = "Pace",
        //        Nome = "Giovanni"
        //    };

        //    HttpContext.Response.Cookies.Append("persona", JsonConvert.SerializeObject(giovanni) );

        //    return Ok(new { Status = "success" });
        //}

        //public ActionResult VerifyCookieObject()
        //{
        //    Persona temp = JsonConvert.DeserializeObject<Persona>(HttpContext.Request.Cookies["persona"]);

        //    return Ok(new { Status = "success" });
        //}

        public IActionResult EffettuaLogin()
        {
            return View();
        }

        [HttpPost]
        public RedirectResult LoginCheck(Credenziali objCred)
        {
            if (objCred.Username.Equals("gioTastic") && objCred.Password.Equals("pace"))
            {
                HttpContext.Response.Cookies.Append("isLogged", objCred.Username);
                HttpContext.Response.Cookies.Append("role", "USER");
                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/EffettuaLogin");
            }

        }

        public IActionResult Index()
        {
            if(HttpContext.Request.Cookies["isLogged"] == null)
                return Redirect("/Home/EffettuaLogin");

            return View();
        }
    }
}
