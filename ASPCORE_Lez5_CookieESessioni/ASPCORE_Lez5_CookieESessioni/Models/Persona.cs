﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez5_CookieESessioni.Models
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
    }
}
