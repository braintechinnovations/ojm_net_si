﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez5_CookieESessioni.Models
{
    public class Credenziali
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
