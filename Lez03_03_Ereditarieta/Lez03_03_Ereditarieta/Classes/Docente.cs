﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_03_Ereditarieta.Classes
{
    class Docente : Persona
    {
        public string Dipartimento { get; set; }

        public Docente()
        {

        }

        public Docente(string varNome, string varCognome, string varCodFis, string varDipartimento)
        {
            Nome = varNome;
            Cognome = varCognome;
            CodFis = varCodFis;
            Dipartimento = varDipartimento;
        }

        public override void stampaDettaglio()
        {
            Console.WriteLine($"{Nome} {Cognome} - Cod.Fis: {CodFis} - Dipartimento: {Dipartimento}");
        }

    }
}
