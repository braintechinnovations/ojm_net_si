﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_03_Ereditarieta.Classes
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodFis { get; set; }

        public Persona()
        {

        }

        public Persona(string varNome, string varCognome)
        {
            this.Nome = varNome;
            this.Cognome = varCognome;
        }

        public virtual void stampaDettaglio()
        {
            Console.WriteLine($"{Nome} {Cognome} - Cod.Fis: {CodFis}");
        }


    }
}
