﻿using DB_02_RelazioneOneToMany.DAL;
using DB_02_RelazioneOneToMany.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_02_RelazioneOneToMany.Controllers
{
    class PersonaController
    {
        private static IConfiguration configuration;

        public PersonaController()
        {
            if (configuration == null)
            {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json", false, false);

                configuration = builder.Build();
            }
        }

        public void InserisciPersona(
            string varNome, 
            string varCognome,
            string varIndirizzo, 
            string varEmail)
        {
            Persona temp = new Persona()
            {
                Cognome = varCognome,
                Nome = varNome,
                Email = varEmail,
                Indirizzo = varIndirizzo
            };

            if(new PersonaDAL(configuration).Insert(temp))
            {
                Console.WriteLine("Stappooo");
            }
            else
            {
                Console.WriteLine("Errore ;(");
            }
            //Creo la persona sul DB
        }
    }
}
