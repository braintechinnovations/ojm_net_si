﻿using DB_02_RelazioneOneToMany.DAL;
using DB_02_RelazioneOneToMany.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_02_RelazioneOneToMany.Controllers
{
    class CartaController
    {
        private static IConfiguration configuration;

        public CartaController()
        {
            if (configuration == null)
            {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json", false, false);

                configuration = builder.Build();
            }
        }

        public void InserisciCarta(string varCodice, string varNegozio, int proprietarioId)
        {
            Carta temp = new Carta();
            temp.Codice = varCodice;
            temp.Negozio = varNegozio;
            temp.Proprietario = new PersonaDAL(configuration).ReadById(proprietarioId);

            CartaDAL cartaDAL = new CartaDAL(configuration);
            cartaDAL.Insert(temp);
        }
    }
}
