﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_02_RelazioneOneToMany.DAL
{
    interface InterfaceDAL<T>
    {
        bool Insert(T t);
        List<T> ReadAll();
        T ReadById(int varId);
        bool Update(T t);
        bool Delete(int varId);
    }
}
