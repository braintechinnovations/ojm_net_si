﻿using DB_02_RelazioneOneToMany.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_02_RelazioneOneToMany.DAL
{
    public class PersonaDAL : InterfaceDAL<Persona>
    {
        private static string stringaConnessione;

        public PersonaDAL(IConfiguration configurazione)
        {
            stringaConnessione = configurazione.GetConnectionString("DatabaseTest");
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Persona t)
        {
            bool risultato = false;

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Persona (nome, cognome, indirizzo, email) "+ 
                    "VALUES (@varNome, @varCognome, @varIndirizzo, @varEmail)";
                comm.Parameters.AddWithValue("@varNome", t.Nome);
                comm.Parameters.AddWithValue("@varCognome", t.Cognome);
                comm.Parameters.AddWithValue("@varIndirizzo", t.Indirizzo);
                comm.Parameters.AddWithValue("@varEmail", t.Email);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    risultato = true;

            }

            return risultato;
        }

        public List<Persona> ReadAll()
        {
            throw new NotImplementedException();
        }

        public Persona ReadById(int varId)
        {
            Persona risultato = null;

            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                string query = "SELECT personaID, nome, cognome, indirizzo, email FROM Persona WHERE personaID = @varId";
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@varId", varId);
                cmd.Connection = c;

                try
                {
                    c.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    reader.Read();

                    Persona temp = new Persona()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Indirizzo = reader[3].ToString(),
                        Email = reader[4].ToString(),
                    };

                    return temp;

                    c.Close();
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SqlException exSql)
                {
                    Console.WriteLine(exSql.Message);
                }

            }


            return risultato;
        }

        public bool Update(Persona t)
        {
            throw new NotImplementedException();
        }
    }
}
