﻿using DB_02_RelazioneOneToMany.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_02_RelazioneOneToMany.DAL
{
    public class CartaDAL : InterfaceDAL<Carta>
    {
        private static string stringaConnessione;

        public CartaDAL(IConfiguration configurazione)
        {
            stringaConnessione = configurazione.GetConnectionString("DatabaseTest");
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Carta t)
        {
            bool risultato = false;

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO CartaFedelta (codiceCarta, negozio, personaRIF) " +
                    "VALUES (@varCodice, @varNegozio, @varPersonaFK)";
                comm.Parameters.AddWithValue("@varCodice", t.Codice);
                comm.Parameters.AddWithValue("@varNegozio", t.Negozio);
                comm.Parameters.AddWithValue("@varPersonaFK", t.Proprietario.Id);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    risultato = true;

            }

            return risultato;
        }

        public List<Carta> ReadAll()
        {
            throw new NotImplementedException();
        }

        public Carta ReadById(int varId)
        {
            throw new NotImplementedException();
        }

        public bool Update(Carta t)
        {
            throw new NotImplementedException();
        }
    }
}
