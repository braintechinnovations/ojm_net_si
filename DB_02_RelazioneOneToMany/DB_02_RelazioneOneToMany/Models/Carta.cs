﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_02_RelazioneOneToMany.Models
{
    public class Carta
    {
        public int Id { get; set; }
        public string Codice { get; set; }
        public string Negozio { get; set; }

        public Persona Proprietario { get; set; }

        public override string ToString()
        {
            return $"CARTA: {Id} - {Codice} - {Negozio} - {Proprietario.DettaglioProp()}";
        }

        public string DettaglioCarta()
        {
            return $"CARTA: {Id} - {Codice} - {Negozio}";
        }
    }
}
