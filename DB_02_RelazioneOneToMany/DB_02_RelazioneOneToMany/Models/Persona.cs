﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_02_RelazioneOneToMany.Models
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Indirizzo { get; set; }
        public string Email { get; set; }

        public List<Carta> ElencoCarte { get; set; }

        public Persona()
        {
            ElencoCarte = new List<Carta>();
        }

        public override string ToString()
        {
            return $"PROP: {Id} - {Nome} - {Cognome} - {stringaLista()}";
        }

        public string DettaglioProp()
        {
            return $"PROP: {Id} - {Nome} - {Cognome}";
        }

        private string stringaLista()
        {
            string risultato = "";

            foreach (Carta item in ElencoCarte)
                risultato += item.DettaglioCarta() + "\n";

            return risultato;
        }
    }
}
