﻿using Lez04_01_StaticoGenerale.Classes;
using System;

namespace Lez04_01_StaticoGenerale
{
    class Program
    {
        static void Main(string[] args)
        {

            float a = 5.0f;
            float b = 6.0f;

            Console.WriteLine(Calcolatrice.faiSomma(a, b));
            Console.WriteLine(Calcolatrice.faiSottrazione(a, b));

            //Calcolatrice calc = new Calcolatrice();     //Non posso creare una istanza di classe Statica
        }
    }
}
