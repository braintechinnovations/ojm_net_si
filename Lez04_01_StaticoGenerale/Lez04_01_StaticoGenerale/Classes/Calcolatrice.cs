﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_01_StaticoGenerale.Classes
{
    public static class Calcolatrice
    {
        public static float faiSomma(float varUno, float varDue)
        {
            return varUno + varDue;
        }
        public static float faiSottrazione(float varUno, float varDue)
        {
            return varUno - varDue;
        }
    }
}
